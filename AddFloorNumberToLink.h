#ifndef ADDFLOORNUMBERTOLINK_H
#define ADDFLOORNUMBERTOLINK_H

#include <QWidget>
#include "NdsCommon.h"
#include <QButtonGroup>
#include <QMessageBox>

static int floorNumber = 0;

namespace Ui {
class AddFloorNumberToLink;
}

class AddFloorNumberToLink : public QWidget
{
    Q_OBJECT

public:
    explicit AddFloorNumberToLink(QWidget *parent = nullptr);
    ~AddFloorNumberToLink();

    /**
     * @brief   设置上下文
     * @param   NdsCommon 对象指针
     * @return
     */
    void    SetContext(NdsCommon * pNdsCommon);

    /**
     * @brief   AddFloorNumberToLink 的操作业务逻辑
     * @param   tileId               tileId
     * @param   linkId               linkId
     * @param   refType              refType
     * @return
     */
    void    DoWork(int tileId, int linkId,int refType);

    /**
     * @brief   创建一个 attrValList 实例
     * @param   entryAttrTypeRef   一个AttrType的引用
     * @return
     */
    nds::common::flexattr::attrmaps::AttrValueList &BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef);


private:
    Ui::AddFloorNumberToLink *ui;
    nds::common::flexattr::attrmaps::AttributeLayer *   m_pRoutingAuxTile =nullptr ;
    nds::routing::main::RoutingTile *                   m_pRoutingTile = nullptr;

    NdsCommon *         m_pNdsCommon = nullptr ;
    QButtonGroup *      m_pGroupBtn = nullptr;


    int m_iTileId = 0;
    int m_iLinkId = 0;

private slots:
     // RadioButton 的槽函数
     void slotsRadioClick();
     void on_floorNumberSpinBox_valueChanged(int arg1);
};

#endif // ADDFLOORNUMBERTOLINK_H
