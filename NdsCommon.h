#ifndef NDSCOMMON_H
#define NDSCOMMON_H

#include <QString>
#include "CppSQLite3/Database.h"
#include "CppSQLite3/Query.h"
#include "datascript/DbAccessMode.h"
#include "datascript/Util.h"
#include "nds/all/ExtParams.h"

#ifndef DATASCRIPT_RUNTIME_INCLUDE_VALIDATION
#define DATASCRIPT_RUNTIME_INCLUDE_VALIDATION
#endif


#include <nds/common/flexattr/attrmaps/AttributeLayer.h>
#include "nds/routing/main/RoutingTile.h"
#include "nds/routing/main/RoutingTileTable.h"

static int selTableId = 0;
static QString SelTableName = "";
static bool isInclusive = true;

class NdsCommon
{
public:
    NdsCommon(nds::routing::main::RoutingTile *pRoutingTile ,
              nds::common::flexattr::attrmaps::AttributeLayer *pRoutingAuxTile ,
              CppSQLite3::Database *pDBHandle);

    int FindAttrMapEntryIdx(datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals,int refType);
    int FindVal4xxxFeatureEntryIdx(int nMap,ObjectArray< nds::common::flexattr::valuecodes::AttributeValue >&attrVal);

    bool Update2Database(QString dbName, QString filedName, int tileId);

    nds::routing::main::RoutingTile * GetRoutingTile();
    nds::common::flexattr::attrmaps::AttributeLayer * GetRoutingAuxTile();

    /**
     * @brief   创建一个 values4ManyFeatures 的实体
     * @param   entry       AttrVals4OneFeatures 对象的引用
     * @param   refAttrMap  attrMap[nMap] 的引用
     * @param   nMap
     * @return
     */
    void BuildAttrVals4ManyFeature(nds::common::flexattr::attrmaps::AttrVals4ManyFeatures &entry,
                                   nds::common::flexattr::attrmaps::AttributeMap &refAttrMap, int nMap,
                                   nds::common::flexattr::attrmaps::FeatureReference &feature,
                                   datascript::ObjectArray <nds::common::flexattr::valuecodes::AttributeValue> &attrVals);

    nds::common::flexattr::attrmaps::FeatureReference &BuildFeature(int linkId, nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef);

    bool AddEntry(int tileId,int nMap,
                  datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals,
                  nds::common::flexattr::attrmaps::FeatureReference &feature,
                  nds::common::flexattr::attrmaps::AttrValueList &attrValList);

    void BuildAttrVals4OneFeature(int tileId,nds::common::flexattr::attrmaps::AttrVals4OneFeature &entry,datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals);

    void BuildAttrVals4ManyFeature(nds::common::flexattr::attrmaps::AttrVals4ManyFeatures &entry,
                                   nds::common::flexattr::attrmaps::AttributeMap &refAttrMap, int nMap);

    void BuildAttrTypeRef(nds::common::flexattr::attrmaps::AttributeTypeRef & entryAttrTypeRef,
                          datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals);

    void BuildAttrMap(nds::common::flexattr::attrmaps::AttributeMap &entryAttrMap,
                      nds::common::flexattr::attrmaps::AttrMapType & attrMapType,
                      nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef);

    void SetAttrMapListInstance( nds::common::flexattr::attrmaps::AttributeMapList * pAttrMapListInstance);

    int AddFeature(int nMap, int insIdx, int nEntry, int linkId);

    CppSQLite3::Database *                                  m_pDBIns = nullptr;

private:
    nds::routing::main::RoutingTile *                       m_pRoutingTile = nullptr;
    nds::common::flexattr::attrmaps::AttributeLayer *       m_pRoutingAuxTile = nullptr;
    nds::common::flexattr::attrmaps::AttributeMapList *     m_pAttrMapListInstance;

};

#endif // NDSCOMMON_H
