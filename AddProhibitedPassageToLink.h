#ifndef ADDPROHIBITEDPASSAGETOLINK_H
#define ADDPROHIBITEDPASSAGETOLINK_H
#include "NdsCommon.h"

#include <QWidget>
#include <QButtonGroup>
#include <QDebug>
#include <QMessageBox>

static int facilityTypeCurIdx = 0;
static int inOutTypeCurIdx = 0;

static nds::common::flexattr::attrdefs::TimeOfDay startTimeOfDay;
static nds::common::flexattr::attrdefs::TimeOfDay endTimeOfDay;

namespace Ui {
class AddProhibitedPassageToLink;
}

class AddProhibitedPassageToLink : public QWidget
{
    Q_OBJECT

public:
    explicit AddProhibitedPassageToLink(QWidget *parent = nullptr);
    ~AddProhibitedPassageToLink();

    /**
     * @brief   AddParkingToLink 的操作业务逻辑
     * @param   tileId               tileId
     * @param   linkId               linkId
     * @param   refType              refType
     * @return
     */
    void DoWork(int tileId, int linkId, int refType);

    /**
     * @brief   设置上下文
     * @param   NdsCommon 对象指针
     * @return
     */
    void SetContext(NdsCommon * pNdsCommon);

    int AddFeature2Tile(int nMap, int nEntry, int linkId);
    int AddFeature2AuxTile(int nMap, int nEntry, int linkId);

    void    BuildAttrVals4OneFeature(nds::common::flexattr::attrmaps::AttrVals4OneFeature &entry);
    /**
     * @brief   创建一个 attrValList 实例
     * @param   entryAttrTypeRef   一个AttrType的引用
     * @return
     */
    nds::common::flexattr::attrmaps::AttrValueList &    BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef);
    nds::common::flexattr::attrmaps::FeatureReference   BuildFeature(int linkId,nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef);


private:
    Ui::AddProhibitedPassageToLink *ui;


    nds::common::flexattr::attrmaps::AttributeLayer *   m_pRoutingAuxTile =nullptr ;
    nds::routing::main::RoutingTile *                   m_pRoutingTile = nullptr;
    NdsCommon *                                         m_pNdsCommon = nullptr ;

    QButtonGroup * m_pSelTabGroupBtn = nullptr;
    QButtonGroup * m_pIsInclusiveGroupBtn = nullptr;

    int m_iTileId = 0;
    int m_iLinkId = 0;

private slots:
    void slotsSelTabRadioClick();
    void slotSelIsIncluRadioClick();
    void on_startTimeEdit_dateTimeChanged(const QDateTime &dateTime);
    void on_endTimeEdit_dateTimeChanged(const QDateTime &dateTime);
};

#endif // ADDPROHIBITEDPASSAGETOLINK_H
