#include "Display.h"


Display::Display(QWidget *parent, Ui::Widget *ui,
                 nds::routing::main::RoutingTile *pRoutingTile,
                 nds::common::flexattr::attrmaps::AttributeLayer * pRoutingAuxTile) : QWidget(parent)
{
    this->ui = ui;
    this->m_pRoutingTile = pRoutingTile;
    this->m_pRoutingAuxTile = pRoutingAuxTile;
}

QTreeWidgetItem *Display::QTreeWidget_addItem(QTreeWidgetItem *parent, QString name, QString value)
{
    QStringList list;
    list.push_back(name);
    list.push_back(value);

    QTreeWidgetItem *item = new QTreeWidgetItem(parent, list);
    return item;
}

QTreeWidgetItem *Display::QTreeWidget_addParentItem(QString name)
{
    QStringList list;
    list.push_back(name);

    QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget, list);
    return item;
}

void Display::ShowRoutingTile()
{
    QTreeWidgetItem *header_item = QTreeWidget_addParentItem("Header");
    nds::routing::main::RoutingTileHeader header = m_pRoutingTile->getHeader();
    QTreeWidgetItem *header_contentMask_item = QTreeWidget_addItem(header_item, "contentMask");

    QTreeWidget_addItem(header_contentMask_item, "hasFixedRoadAttributeSetList",QString::number(header.getContentMask().getHasFixedRoadAttributeSetList()));
    QTreeWidget_addItem(header_contentMask_item, "hasSimpleIntersectionList",QString::number(header.getContentMask().getHasSimpleIntersectionList()));
    QTreeWidget_addItem(header_contentMask_item, "hasLinkList",QString::number(header.getContentMask().getHasLinkList()));
    QTreeWidget_addItem(header_contentMask_item, "hasRouteUpLinkList",QString::number(header.getContentMask().getHasRouteUpLinkList()));
    QTreeWidget_addItem(header_contentMask_item, "hasRouteDownLinkList",QString::number(header.getContentMask().getHasRouteDownLinkList()));
    QTreeWidget_addItem(header_contentMask_item, "hasLinkToTileList",QString::number(header.getContentMask().getHasLinkToTileList()));
    QTreeWidget_addItem(header_contentMask_item, "hasLinkIdRangeList",QString::number(header.getContentMask().getHasLinkIdRangeList()));
    QTreeWidget_addItem(header_contentMask_item, "hasSimpleIntersectionIdRangeList",QString::number(header.getContentMask().getHasSimpleIntersectionIdRangeList()));
    QTreeWidget_addItem(header_contentMask_item, "hasAttributeMaps",QString::number(header.getContentMask().getHasAttributeMaps()));
    QTreeWidget_addItem(header_contentMask_item, "hasExternalTileIdList",QString::number(header.getContentMask().getHasExternalTileIdList()));

    if (header.getContentMask().getHasFixedRoadAttributeSetList()) {
        QTreeWidget_addItem(header_item, "fixedRoadAttributeSetListOffset",QString::number(header.getFixedRoadAttributeSetListOffset()));
        // Analysis FixedRoadAttributeSetList
        AnalysisFixedRoadAttributeSetList();
    }
    if (header.getContentMask().getHasSimpleIntersectionList()) {
        QTreeWidget_addItem(header_item, "simpleIntersectionOffset",QString::number(header.getSimpleIntersectionOffset()));
        // Analysis SimpleIntersectionList
        AnalysisSimpleIntersectionList();

    }
    if (header.getContentMask().getHasLinkList()) {
        QTreeWidget_addItem(header_item, "linksOffset", QString::number(header.getLinksOffset()));
        // Analysis LinkList
        AnalysisLinkList();
    }
    if (header.getContentMask().getHasRouteUpLinkList()) {
        QTreeWidget_addItem(header_item, "routeUpLinksOffset", QString::number(header.getRouteUpLinksOffset()));
        // Analysis RouteUpLinkList
        AnalysisRouteUpLinkList();
    }
    if (header.getContentMask().getHasRouteDownLinkList()) {
        QTreeWidget_addItem(header_item, "routeDownLinksOffset", QString::number(header.getRouteDownLinksOffset()));
        // Analysis RouteDownLinkList
        AnalysisRouteDownLinkList();
    }
    if (header.getContentMask().getHasLinkToTileList()) {
        QTreeWidget_addItem(header_item, "link2TileListOffset", QString::number(header.getLink2TileListOffset()));
        // Analysis LinkToTileList
    }
    if (header.getContentMask().getHasLinkIdRangeList()) {
        QTreeWidget_addItem(header_item, "attributeMapsOffset", QString::number(header.getLink2TileListOffset()));
        // Analysis LinkIdRangeList
    }
    if (header.getContentMask().getHasSimpleIntersectionIdRangeList()) {
        QTreeWidget_addItem(header_item, "attributeMapsOffset", QString::number(header.getSimplIntIdRangeListOffset()));
        // Analysis SimpleIntersectionIdRangeList
    }
    if (header.getContentMask().getHasAttributeMaps()) {
        QTreeWidget_addItem(header_item, "attributeMapsOffset", QString::number(header.getAttributeMapsOffset()));
        // Analysis AttributeMaps
        AnalysisAttributeMapList();
    }
    if (header.getContentMask().getHasExternalTileIdList()) {
        QTreeWidget_addItem(header_item, "externalTileIdListOffset",
                            QString::number(header.getExternalTileIdListOffset()));
        // Analysis ExternalTileIdList
        AnalysisExternalTileIdList();
    }
}

void Display::ShowRoutingAuxTile()
{
    QTreeWidgetItem * hasExternalTileIdList_item = QTreeWidget_addParentItem("hasExternalTileIdList");
    QTreeWidget_addItem(hasExternalTileIdList_item,"hasExternalTileIdList",QString::number(m_pRoutingAuxTile->getHasExternalTileIdList()));

    if (m_pRoutingAuxTile->getHasExternalTileIdList())
    {
        QTreeWidgetItem * externalTileIdList = QTreeWidget_addParentItem("externalTileIdList");
        QTreeWidget_addItem(externalTileIdList,"numTileIds",QString::number(m_pRoutingAuxTile->getExternalTileIdList().getNumTileIds()));
        QTreeWidgetItem * tileId_item = QTreeWidget_addItem(externalTileIdList,"tileId");
        for (int i = 0; i < m_pRoutingAuxTile->getExternalTileIdList().getNumTileIds(); i++)
        {
            QString str = QString("tile[%1]").arg(i);
            QTreeWidget_addItem(tileId_item,str,QString::number(m_pRoutingAuxTile->getExternalTileIdList().getTileId().elementAt(i)));
        }
    }

    AnalysisAttributeMapList();
}

void Display::AnalysisFixedRoadAttributeSetList()
{
    // 添加 FixedRoadAttributeSetList 节点
    QTreeWidgetItem *FixedRoadAttributeSetList_item = QTreeWidget_addParentItem("FixedRoadAttributeSetList");
    nds::routing::attribute::FixedRoadAttributeSetList fixRoAttrSetList = m_pRoutingTile->getFixedRoadAttributeSetList();
    int count = fixRoAttrSetList.getNumAttributeSets();
    QTreeWidget_addItem(FixedRoadAttributeSetList_item, "numAttributesSets", QString::number(count));
    QTreeWidgetItem *numAttributesList_item = QTreeWidget_addItem(FixedRoadAttributeSetList_item, "attributeList");
    datascript::ObjectArray <nds::routing::attribute::FixedRoadAttributeSet> list = fixRoAttrSetList.getAttributeList();
    for (int i = 0; i < count; i++) {
        QString str = QString("attributeList[%1]").arg(i);
        QTreeWidgetItem *attributeList_item = QTreeWidget_addItem(numAttributesList_item, str);

        nds::routing::attribute::FixedRoadAttributeSet fixedattr = list.elementAt(i);
        QTreeWidgetItem *sharedAttr_item = QTreeWidget_addItem(attributeList_item, "sharedAttr");
        QTreeWidget_addItem(sharedAttr_item, "priorityRoadClass",QString::number(fixedattr.getSharedAttr().getPriorityRoadClass()));
        QTreeWidget_addItem(sharedAttr_item, "linkType",QString::number(fixedattr.getSharedAttr().getLinkType().getValue()));
        QTreeWidget_addItem(sharedAttr_item, "travelIDirection",QString::number(fixedattr.getSharedAttr().getTravelDirection().getValue()));
        QTreeWidget_addItem(sharedAttr_item, "ferry", QString::number(fixedattr.getSharedAttr().getFerry()));
        QTreeWidget_addItem(sharedAttr_item, "tunnel", QString::number(fixedattr.getSharedAttr().getTunnel()));
        QTreeWidget_addItem(sharedAttr_item, "bridge", QString::number(fixedattr.getSharedAttr().getBridge()));
        QTreeWidget_addItem(sharedAttr_item, "toll",QString::number(fixedattr.getSharedAttr().getToll().getValue()));
        QTreeWidget_addItem(sharedAttr_item, "controlledAccess",QString::number(fixedattr.getSharedAttr().getControlledAccess()));
        QTreeWidget_addItem(sharedAttr_item, "serviceArea",QString::number(fixedattr.getSharedAttr().getServiceArea()));

        QTreeWidgetItem *routingAttr_item = QTreeWidget_addItem(attributeList_item, "routingAttr");
        QTreeWidget_addItem(routingAttr_item, "functionalRoadClass",QString::number(fixedattr.getRoutingAttr().getFunctionalRoadClass()));
        QTreeWidget_addItem(routingAttr_item, "urban", QString::number(fixedattr.getRoutingAttr().getUrban()));
        QTreeWidget_addItem(routingAttr_item, "complexIntersection",QString::number(fixedattr.getRoutingAttr().getComplexIntersection()));
        QTreeWidget_addItem(routingAttr_item, "pluraljunction",QString::number(fixedattr.getRoutingAttr().getPluralJunction()));
        QTreeWidget_addItem(routingAttr_item, "motorway",QString::number(fixedattr.getRoutingAttr().getMotorway()));
    }
}

void Display::AnalysisSimpleIntersectionList()
{
    // 添加 SimpleIntersection 节点
    QTreeWidgetItem *SimpleIntersectionList_item = QTreeWidget_addParentItem("SimpleIntersection");
    nds::routing::intersection::SimpleIntersectionList simpleinterList = m_pRoutingTile->getSimpleIntersection();
    int count = simpleinterList.getNumIntersections();
    QTreeWidget_addItem(SimpleIntersectionList_item, "numIntersections", QString::number(count));
    QTreeWidgetItem *numIntersections_item = QTreeWidget_addItem(SimpleIntersectionList_item, "attributeList");
    datascript::ObjectArray <nds::routing::intersection::SimpleIntersection> list = simpleinterList.getSimpleIntersection();
    for (int i = 0; i < count; i++) {
        QString str = QString("simpleIntersection[%1]").arg(i);
        nds::routing::intersection::SimpleIntersection simpInter = list.elementAt(i);
        QTreeWidgetItem *attributeList_item = QTreeWidget_addItem(numIntersections_item, str);
        QTreeWidgetItem *positon_item = QTreeWidget_addItem(attributeList_item, "position");
        QTreeWidget_addItem(positon_item, "dx", QString::number(simpInter.getPosition().getDx()));
        QTreeWidget_addItem(positon_item, "dy", QString::number(simpInter.getPosition().getDy()));
        QTreeWidget_addItem(attributeList_item, "numLinks", QString::number(simpInter.getNumLinks()));
        int connLinks = simpInter.getNumLinks();
        QTreeWidgetItem *connLinks_item = QTreeWidget_addItem(attributeList_item, "contectedLinks");
        for (int j = 0; j < connLinks; j++) {
            QString str = QString("contectedLinks[%1]").arg(j);
            QTreeWidgetItem *connLinks_item_p = QTreeWidget_addItem(connLinks_item, str);
            QTreeWidget_addItem(connLinks_item_p, "isExternalLinkReference", QString::number(simpInter.getConnectedLinks().getData()->getIsExternalLinkReference()));
            if (simpInter.getConnectedLinks().getData()->getIsExternalLinkReference())
            {
                nds::common::IntOrExtDirectedLinkReference exterLinkRef = simpInter.getConnectedLinks().elementAt(j);
                QTreeWidgetItem *connLinks_item_linkRefCho = QTreeWidget_addItem(connLinks_item_p, "linkReferenceChoice");
                QTreeWidgetItem *connLinks_item_exterLinkRef = QTreeWidget_addItem(connLinks_item_linkRefCho, "externalLinkReference");
                QTreeWidget_addItem(connLinks_item_exterLinkRef, "extTileIdx", QString::number(
                                        simpInter.getConnectedLinks().getData()->getLinkReferenceChoice().getExternalLinkReference().getExtTileIdx()));
                QTreeWidgetItem *connLinks_item_tileExterLinkRef = QTreeWidget_addItem(connLinks_item_exterLinkRef, "tileExternalLinkReference");
                QTreeWidget_addItem(connLinks_item_tileExterLinkRef, "positiveLinkDirection", QString::number(
                                        simpInter.getConnectedLinks().getData()->getLinkReferenceChoice().getExternalLinkReference().getTileExternalLinkReference().getPositiveLinkDirection()));
            }
            else
            {

            }
        }
    }
}

void Display::AnalysisLinkList()
{
    // 添加 Links 节点
    QTreeWidgetItem *Links_item = QTreeWidget_addParentItem("Links");

    nds::routing::link::LinkList Links = m_pRoutingTile ->getLinks();
    int count = Links.getNumLinks();
    QTreeWidget_addItem(Links_item, "numLinks", QString::number(count));
    // 获取 Tile 中所有的 link,放在一个数组中
    QTreeWidgetItem *link_item = QTreeWidget_addItem(Links_item, "link");
    datascript::ObjectArray <nds::routing::link::Link> list = Links.getLink();
    for (int i = 0; i < count; i++) {
        nds::routing::link::Link link = list.elementAt(i);
        QString str = QString("link[%1]").arg(i);
        QTreeWidgetItem *numLinks_item = QTreeWidget_addItem(link_item, str);

        QTreeWidget_addItem(numLinks_item, "length", QString::number(link.getLength()));
        QTreeWidget_addItem(numLinks_item, "attrSource", QString::fromStdString(link.getAttrSource().toString()));
        QTreeWidgetItem *numLinks_attrInfo_item = QTreeWidget_addItem(numLinks_item, "attrInfo");
        QTreeWidget_addItem(numLinks_attrInfo_item, "fixedRoadAttributeSetListIndex",
                            QString::number(link.getAttrInfo().getFixedRoadAttributeSetListIndex()));
        QTreeWidget_addItem(numLinks_item, "averageSpeed", QString::number(link.getAverageSpeed()));
        QTreeWidget_addItem(numLinks_item, "startAngle", QString::number(link.getStartAngle()));
        QTreeWidget_addItem(numLinks_item, "endAngle", QString::number(link.getEndAngle()));
    }

}

void Display::AnalysisRouteUpLinkList()
{

    QTreeWidgetItem *routeUpLinks_item = QTreeWidget_addParentItem("routeUpLinks");
    nds::routing::ref::RouteUpLinkList routeUpLinks = m_pRoutingTile->getRouteUpLinks();

    int count = routeUpLinks.getNumRefs();
    QTreeWidget_addItem(routeUpLinks_item, "numLinks", QString::number(count));
    QTreeWidgetItem *routeUpLinkRef_item = QTreeWidget_addItem(routeUpLinks_item, "routeUpLinkRef");

    datascript::ObjectArray <nds::routing::ref::RouteUpLinkRef> list = routeUpLinks.getRouteUpLinkRef();
    for (int i = 0; i < count; i++) {
        nds::routing::ref::RouteUpLinkRef upLink = list.elementAt(i);
        QString str = QString("routeUpLinkRef[%1]").arg(i);
        QTreeWidgetItem *routeUpLinkRef_item_part = QTreeWidget_addItem(routeUpLinkRef_item, str);

        QTreeWidgetItem *routeUpLinkRef_item_fromref_item = QTreeWidget_addItem(routeUpLinkRef_item_part, "fromLink");
        QTreeWidget_addItem(routeUpLinkRef_item_fromref_item, "positiveLinkDirection",
                            QString::number(upLink.getFromLink().getPositiveLinkDirection()));
        QTreeWidget_addItem(routeUpLinkRef_item_fromref_item, "LinkId",
                            QString::number(upLink.getFromLink().getLinkId()));
        QTreeWidgetItem *routeUpLinkRef_item_toref_item = QTreeWidget_addItem(routeUpLinkRef_item_part, "toLink");

        QTreeWidget_addItem(routeUpLinkRef_item_toref_item, "extTileIdx", QString::number(upLink.getToLink().getExtTileIdx()));
        QTreeWidgetItem *routeUpLinkRef_item_toref_item_tileExterLinkRef = QTreeWidget_addItem(
                    routeUpLinkRef_item_toref_item, "tileExternalLinkReference");
        QTreeWidget_addItem(routeUpLinkRef_item_toref_item_tileExterLinkRef, "positiveLinkDirection",
                            QString::number(upLink.getToLink().getTileExternalLinkReference().getPositiveLinkDirection()));
        QTreeWidget_addItem(routeUpLinkRef_item_toref_item_tileExterLinkRef, "linkId",
                            QString::number(upLink.getToLink().getTileExternalLinkReference().getLinkId()));
    }

}

void Display::AnalysisRouteDownLinkList()
{

    QTreeWidgetItem *routeDownLinks_item = QTreeWidget_addParentItem("routeDownLinks");
    nds::routing::ref::RouteDownLinkList routeDownLinks = m_pRoutingTile->getRouteDownLinks();

    int count = routeDownLinks.getNumRouteLinks();
    QTreeWidget_addItem(routeDownLinks_item, "numRouteLinks", QString::number(count));
    QTreeWidgetItem *routeDownLinkRef_item = QTreeWidget_addItem(routeDownLinks_item, "routeDownLinkElement");

    datascript::ObjectArray <nds::routing::ref::RouteDownLinkElement> list = routeDownLinks.getRouteDownLinkElement();
    for (int i = 0; i < count; i++) {
        nds::routing::ref::RouteDownLinkElement downLink = list.elementAt(i);
        QString str = QString("routeUpLinkRef[%1]").arg(i);
        QTreeWidgetItem *routeDownLinkEle_item_part = QTreeWidget_addItem(routeDownLinkRef_item, str);
        QTreeWidget_addItem(routeDownLinkEle_item_part, "numRefs", QString::number(downLink.getNumRefs()));
        QTreeWidgetItem *routeDownLinkEle_item_toLink_item = QTreeWidget_addItem(routeDownLinkEle_item_part,
                                                                                 "toLink");
        datascript::ObjectArray <nds::common::ExternalDirectedLinkReference> toLinkList = downLink.getToLink();
        for (int j = 0; j < downLink.getNumRefs(); j++) {
            nds::common::ExternalDirectedLinkReference toLinkListExt = toLinkList.elementAt(j);
            QTreeWidgetItem *routeDownLinkEle_item_toLink_item_part = QTreeWidget_addItem(routeDownLinkEle_item_toLink_item, QString("toLink[%1]").arg(j));
            QTreeWidget_addItem(routeDownLinkEle_item_toLink_item_part, "extTileIdx", QString::number(toLinkListExt.getExtTileIdx()));

            QTreeWidgetItem *routeDownLinkEle_item_toLink_item_part_tileExt = QTreeWidget_addItem(
                        routeDownLinkEle_item_toLink_item_part, QString("TileExternalLinkReference"));
            QTreeWidget_addItem(routeDownLinkEle_item_toLink_item_part_tileExt, "PositiveLinkDirection",
                                QString::number(toLinkListExt.getTileExternalLinkReference().getPositiveLinkDirection()));
            QTreeWidget_addItem(routeDownLinkEle_item_toLink_item_part_tileExt, "linId",
                                QString::number(toLinkListExt.getTileExternalLinkReference().getLinkId()));
        }
    }
}

void Display::AnalysisAttributeMapList()
{
    delete m_pAttributeMapItem;

    m_pAttributeMapItem = QTreeWidget_addParentItem("attributeMaps");
    int count = m_pAttrMapListInstance->getNumMaps();
    QTreeWidget_addItem(m_pAttributeMapItem, "numMaps", QString::number(count));

    QTreeWidgetItem *attrTypeRef_item = QTreeWidget_addItem(m_pAttributeMapItem, "attrTypeRef");
    datascript::ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &attrTypelist = m_pAttrMapListInstance -> getAttrTypeRef();

    for (int i = 0; i < count; i++)
    {
        nds::common::flexattr::attrmaps::AttributeTypeRef &attrMapRef = attrTypelist.elementAt(i);
        QTreeWidgetItem *attrTypeRef_part = QTreeWidget_addItem(attrTypeRef_item, QString("attrTypeRef[%1]").arg(i));
        QTreeWidget_addItem(attrTypeRef_part, "numAttrCodes", QString::number(attrMapRef.getNumAttrCodes()));
        QTreeWidgetItem *attrTypeCodes_item = QTreeWidget_addItem(attrTypeRef_part, QString("attributeTypeCodes"));

        datascript::ObjectArray <nds::common::flexattr::valuecodes::AttributeTypeCode> &attrTypeCodeList = attrMapRef.getAttributeTypeCodes();
        for (int j = 0; j < attrMapRef.getNumAttrCodes(); j++)
        {
            nds::common::flexattr::valuecodes::AttributeTypeCode &attrTypeCodeEle = attrTypeCodeList.elementAt(j);
            QTreeWidgetItem *attrTypeCodes_part_item = QTreeWidget_addItem(attrTypeCodes_item, QString("attributeTypeCodes[%1]").arg(j));
            QTreeWidget_addItem(attrTypeCodes_part_item, "AttributeTypeCode", QString::number(attrTypeCodeEle.getValue()));
        }
        QTreeWidget_addItem(attrTypeRef_part, "referenceType", QString::number(attrMapRef.getReferenceType().getValue()));
        QTreeWidget_addItem(attrTypeRef_part, "attrTypeOffset", QString::number(attrMapRef.getAttrTypeOffset()));
    }

    datascript::ObjectArray <nds::common::flexattr::attrmaps::AttributeMap> listMap = m_pAttrMapListInstance -> getAttrMap();
    QTreeWidgetItem *attrMap_item = QTreeWidget_addItem(m_pAttributeMapItem, "attrMap");
    for (int i = 0; i < count; i++)
    {
        nds::common::flexattr::attrmaps::AttributeMap attrMap = listMap.elementAt(i);
        QTreeWidgetItem *attrMap_part = QTreeWidget_addItem(attrMap_item, QString("attrMap[%1]").arg(i));
        QTreeWidget_addItem(attrMap_part, "attrMapType", QString::number(attrMap.getAttrMapType().getValue()));
        QTreeWidget_addItem(attrMap_part, "numEntries", QString::number(attrMap.getNumEntries()));
        QTreeWidgetItem *value4Fea_item = nullptr;

        if (attrMap.getAttrMapType().getValue() == nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE)
        {
            value4Fea_item = QTreeWidget_addItem(attrMap_part, "values4OneFeature");
            datascript::ObjectArray <nds::common::flexattr::attrmaps::AttrVals4OneFeature> feaList = attrMap.getValues4OneFeature();
            for (int j = 0; j < feaList.getSize(); j++)
            {
                nds::common::flexattr::attrmaps::AttrVals4OneFeature feaEle = feaList.elementAt(j);
                QTreeWidgetItem *value4OneFea_item_part = QTreeWidget_addItem(value4Fea_item,QString("values4OneFeatures[%1]").arg(j));
                QTreeWidgetItem *feature_item = QTreeWidget_addItem(value4OneFea_item_part, QString("feature"));

                switch (attrTypelist.elementAt(i).getReferenceType().getValue())
                {
                case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_SIMPLE_INTERSECTION :
                {
                    break;
                }
                case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_LINK_DIRECTED:
                {
                    QTreeWidget_addItem(feature_item, "positiveLinkDirection", QString::number(feaEle.getFeature().getDirectedLinkRef().getPositiveLinkDirection()));
                    QTreeWidget_addItem(feature_item, "linkId",QString::number(feaEle.getFeature().getDirectedLinkRef().getLinkId()));
                    break;
                }
                case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_LINK_BOTH_DIRECTIONS:
                {
                    QTreeWidget_addItem(feature_item, "linkId", QString::number(feaEle.getFeature().getLinkId()));
                    break;
                }
                    // TODO ????????
                    //                case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_ROAD_GEO_LINE_DIRECTED:
                    //                {
                    //                    QTreeWidget_addItem(feature_item, "roadGeoLinkId", QString::number(feaEle.getFeature().getRoadGeoLineId()));
                    //                    break;
                    //                }

                case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_SIMPLE_INTERSECTION_TRANSITION:
                {
                    QTreeWidgetItem *transRefSimpInter_item = QTreeWidget_addItem(feature_item,"transitionReferenceSimpleIntersection");
                    QTreeWidget_addItem(transRefSimpInter_item,"simpleInterscetionId",QString::number(feaEle.getFeature().getTransitionReferenceSimpleIntersections().getSimpleIntersectionId()));
                    QTreeWidget_addItem(transRefSimpInter_item,"transitionOrfinalNumber",QString::number(feaEle.getFeature().getTransitionReferenceSimpleIntersections().getTransitionOrdinalNumber()));
                    break;
                }
                case nds::common::flexattr::attrmaps::ReferenceType::BMD_AREA_FEATURE:
                {
                    break;
                }
                case nds::common::flexattr::attrmaps::ReferenceType::BMD_LINE_FEATURE_DIRECTED:
                {
                    break;
                }
                case nds::common::flexattr::attrmaps::ReferenceType::BMD_LINE_FEATURE_BOTH_DIRECTIONS:
                {

                    break;
                }
                case nds::common::flexattr::attrmaps::ReferenceType::BMD_POINT_FEATURE:
                {

                    break;
                }
                case nds::common::flexattr::attrmaps::ReferenceType::ALL_TILE_FEATURES:
                {

                    break;
                }
                }

                QTreeWidgetItem *attrValList = QTreeWidget_addItem(value4OneFea_item_part, QString("attrValList"));
                QTreeWidgetItem *atteValList_item = QTreeWidget_addItem(attrValList, QString("values"));
                datascript::ObjectArray <nds::common::flexattr::valuecodes::AttributeValue> attrValList_part = feaEle.getAttrValList().getValues();
                for (int k = 0; k < attrTypelist.elementAt(i).getNumAttrCodes(); k++) {
                    QTreeWidgetItem *attrValList_item_part = QTreeWidget_addItem(atteValList_item, QString("values[%1]").arg(k));
                    switch (attrTypelist.elementAt(i).getAttributeTypeCodes().elementAt(k).getValue())
                    {
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::NUM_TRAFFIC_LIGHTS:
                    {
                        QTreeWidget_addItem(attrValList_item_part, "numTrafficLights", QString::number(attrValList_part.elementAt(k).getNumTrafficLights()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::SPEED_LIMIT:
                    {
                        QTreeWidget_addItem(attrValList_item_part, "speedLimit", QString::number(attrValList_part.elementAt(k).getSpeedLimit()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::TRANSITION_MASK_2_4_VAL:
                    {
                        QTreeWidget_addItem(attrValList_item_part, "transtionMask2To4Val", QString::number(attrValList_part.elementAt(k).getTransitionMask2To4Val()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::PARKING:
                    {
                        QTreeWidgetItem *parking_item = QTreeWidget_addItem(attrValList_item_part,"parking");
                        QTreeWidget_addItem(parking_item,"facilityType",QString::number(attrValList_part.elementAt(k).getParking().getFacilityType().getValue()));
                        QTreeWidget_addItem(parking_item,"inOutType",QString::number(attrValList_part.elementAt(k).getParking().getInOutType().getValue()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::LENGTH_ALONG_LINK:
                    {
                        QTreeWidget_addItem(attrValList_item_part, "hahahaha", QString::number(attrValList_part.elementAt(k).getLengthAlongLink()));
                        break;
                    }
                    }
                }
            }
        }
        else if (attrMap.getAttrMapType().getValue() == nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES)
        {
            value4Fea_item = QTreeWidget_addItem(attrMap_part, "values4ManyFeatures");
            datascript::ObjectArray <nds::common::flexattr::attrmaps::AttrVals4ManyFeatures> valManyFeaList = attrMap.getValues4ManyFeatures();
            for (int j = 0; j < valManyFeaList.getSize(); j++)
            {
                nds::common::flexattr::attrmaps::AttrVals4ManyFeatures valManyFeaEle = valManyFeaList.elementAt(j);
                QTreeWidgetItem *value4ManyFea_item_part = QTreeWidget_addItem(value4Fea_item, QString("values4ManyFeatures[%1]").arg(j));
                QTreeWidget_addItem(value4ManyFea_item_part, "numFeatures", QString::number(valManyFeaEle.getNumFeatures()));
                QTreeWidgetItem *feature_item = QTreeWidget_addItem(value4ManyFea_item_part, QString("feature"));

                datascript::ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> feaList = valManyFeaEle.getFeature();
                for (int k = 0; k < feaList.getSize(); k++)
                {
                    QTreeWidgetItem *feature_item_part = QTreeWidget_addItem(feature_item, QString("feature[%1]").arg(k));
                    nds::common::flexattr::attrmaps::FeatureReference feaEle = feaList.elementAt(k);
                    switch (attrTypelist.elementAt(i).getReferenceType().getValue())
                    {
                    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_SIMPLE_INTERSECTION :
                    {
                        QTreeWidget_addItem(feature_item_part, "intersectionRef", QString::number(feaEle.getIntersectionRef()));
                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_LINK_DIRECTED:
                    {
                        QTreeWidget_addItem(feature_item_part, "positiveLinkDirection", QString::number(feaEle.getDirectedLinkRef().getPositiveLinkDirection()));
                        QTreeWidget_addItem(feature_item_part, "linkId", QString::number(feaEle.getDirectedLinkRef().getLinkId()));
                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_LINK_BOTH_DIRECTIONS:
                    {
                        QTreeWidget_addItem(feature_item_part, "linkId", QString::number(feaEle.getLinkId()));
                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_ROAD_GEO_LINE_DIRECTED:
                    {

                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_SIMPLE_INTERSECTION_TRANSITION:
                    {
                        QTreeWidgetItem *transRefSimpInter_item = QTreeWidget_addItem(feature_item_part,"transitionReferenceSimpleIntersection");
                        QTreeWidget_addItem(transRefSimpInter_item,"simpleInterscetionId",QString::number(feaEle.getTransitionReferenceSimpleIntersections().getSimpleIntersectionId()));
                        QTreeWidget_addItem(transRefSimpInter_item,"transitionOrfinalNumber",QString::number(feaEle.getTransitionReferenceSimpleIntersections().getTransitionOrdinalNumber()));
                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::BMD_AREA_FEATURE:
                    {

                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::BMD_LINE_FEATURE_DIRECTED:
                    {

                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::BMD_LINE_FEATURE_BOTH_DIRECTIONS:
                    {

                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::BMD_POINT_FEATURE:
                    {

                        break;
                    }
                    case nds::common::flexattr::attrmaps::ReferenceType::ALL_TILE_FEATURES:
                    {

                        break;
                    }
                    }
                }

                QTreeWidgetItem *attrValList = QTreeWidget_addItem(value4ManyFea_item_part, QString("attrValList"));
                QTreeWidgetItem *atteValList_item = QTreeWidget_addItem(attrValList, QString("values"));
                datascript::ObjectArray <nds::common::flexattr::valuecodes::AttributeValue> attrValList_part = valManyFeaEle.getAttrValList().getValues();
                for (int k = 0; k < attrValList_part.getSize(); k++)
                {
                    QTreeWidgetItem *attrValList_item_part = QTreeWidget_addItem(atteValList_item, QString("values[%1]").arg(k));
                    switch (attrTypelist.elementAt(i).getAttributeTypeCodes().elementAt(k).getValue())
                    {
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::NUM_TRAFFIC_LIGHTS:
                    {
                        QTreeWidget_addItem(attrValList_item_part, "numTrafficLights",QString::number(attrValList_part.elementAt(k).getNumTrafficLights()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::SPEED_LIMIT:
                    {
                        QTreeWidget_addItem(attrValList_item_part,"speedLimit",QString::number(attrValList_part.elementAt(k).getSpeedLimit()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::TRANSITION_MASK_2_4_VAL:
                    {
                        QTreeWidget_addItem(attrValList_item_part, "transtionMask2To4Val", QString::number(attrValList_part.elementAt(k).getTransitionMask2To4Val()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::PARKING:
                    {
                        QTreeWidgetItem *parking_item = QTreeWidget_addItem(attrValList_item_part,"parking");
                        QTreeWidget_addItem(parking_item,"facilityType",QString::number(attrValList_part.elementAt(k).getParking().getFacilityType().getValue()));
                        QTreeWidget_addItem(parking_item,"inOutType",QString::number(attrValList_part.elementAt(k).getParking().getInOutType().getValue()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::PROHIBITED_PASSAGE:
                    {
                        // empty
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::TIME_RANGE_OF_DAY:
                    {
                        QTreeWidgetItem * timeRangeOfDay_item = QTreeWidget_addItem(attrValList_item_part, "timeRangeOfDay");
                        QTreeWidgetItem * startTime_item = QTreeWidget_addItem(timeRangeOfDay_item,"startTime");
                        QTreeWidget_addItem(startTime_item,"hours",QString::number(attrValList_part.elementAt(k).getTimeRangeOfDay().getStartTime().getHours()));
                        QTreeWidget_addItem(startTime_item,"minutes",QString::number(attrValList_part.elementAt(k).getTimeRangeOfDay().getStartTime().getMinutes()));

                        QTreeWidgetItem * endTime_item = QTreeWidget_addItem(timeRangeOfDay_item,"endTime");
                        QTreeWidget_addItem(endTime_item,"hours",QString::number(attrValList_part.elementAt(k).getTimeRangeOfDay().getEndTime().getHours()));
                        QTreeWidget_addItem(endTime_item,"minutes",QString::number(attrValList_part.elementAt(k).getTimeRangeOfDay().getEndTime().getMinutes()));

                        QTreeWidget_addItem(timeRangeOfDay_item,"isInclusive",QString::number(attrValList_part.elementAt(k).getTimeRangeOfDay().getIsInclusive()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::ROAD_Z_LEVEL:
                    {
                        QTreeWidget_addItem(attrValList_item_part, "roadHeightLevel",QString::number(attrValList_part.elementAt(k).getRoadHeightLevel()));
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::VALIDITY_RANGE:
                    {
                        QTreeWidgetItem * validityRange_item = QTreeWidget_addItem(attrValList_item_part, "validityRange");
                        QTreeWidgetItem * startPos_item =  QTreeWidget_addItem(validityRange_item,"startPosition");
                        QTreeWidget_addItem(startPos_item,"numVx4",QString::number(attrValList_part.elementAt(k).getValidityRange().getStartPosition().getNumVx4()));
                        QTreeWidgetItem * endPos_item = QTreeWidget_addItem(validityRange_item,"endPosition");
                        QTreeWidget_addItem(endPos_item,"numVx4",QString::number(attrValList_part.elementAt(k).getValidityRange().getEndPosition().getNumVx4()));
                        break;
                    }

                    case nds::common::flexattr::valuecodes::AttributeTypeCode::START_OR_DESTINATION_ROAD_ONLY:
                    {
                        //empty
                        break;
                    }
                    case nds::common::flexattr::valuecodes::AttributeTypeCode::FLOOR_NUMBER:
                    {
                        QTreeWidget_addItem(attrValList_item_part,"floorNumber",QString::number(attrValList_part.elementAt(k).getFloorNumber()));
                        break;
                    }
                    default:
                    {
                        break;
                    }
                    }
                }
            }
        }
    }
}

void Display::SetAttrMapListInstance(nds::common::flexattr::attrmaps::AttributeMapList *pAttrMapListInstance)
{
    if (pAttrMapListInstance != nullptr)
    {
        m_pAttrMapListInstance = pAttrMapListInstance;
    }
}

void Display::AnalysisExternalTileIdList()
{

    QTreeWidgetItem *exterTileIdList_item = QTreeWidget_addParentItem("externalTileIdList");
    nds::common::ExternalTileIdList exterTileIdList = m_pRoutingTile->getExternalTileIdList();
    int count = exterTileIdList.getNumTileIds();
    QTreeWidget_addItem(exterTileIdList_item, "numTileids", QString::number(count));

    QTreeWidgetItem *tileId_item = QTreeWidget_addItem(exterTileIdList_item, "tileId");

    for (int i = 0; i < count; i++)
    {
        QString str = QString("tileId[%1]").arg(i);
        QTreeWidget_addItem(tileId_item, str, QString::number(exterTileIdList.getTileId().getData()[i]));
    }
}
