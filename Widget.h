#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <map>
#include <vector>
#include <QDebug>
#include <string.h>
#include <QString>
#include <QMessageBox>
#include "AddSpeedLimitToLink.h"
#include "NdsCommon.h"
#include "AddFloorNumberToLink.h"

using namespace std;

#include "Display.h"

namespace Ui
{
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);

    /**
     * @brief   获取 buildingBlockName
     * @param   buildingBlockName       buildingBlockName's name
     * @return  buildingBlock's Id
     */
    int GetBuildingBlockId(QString buildingBlockName);

    /**
     * @brief   根据 buildingBlock's Id 和 tileId 计算 coordWidth
     * @param   buildingBlockName       buildingBlockName's name
     * @return  coordWidth
     */
    int CoordWidth(int buildingBlockId, int tileId);

    /**
     * @brief   初始化数据库链接
     * @param
     * @return
     */
    void InitDatabase();

    /**
     * @brief   读取 RoutingTileTable 的 ndsData 并解析
     * @param   ndsData     读取的数据库中 ndsData
     * @param   nBytes      ndsData length
     * @param   coordWidth  coordWidth
     * @return
     */
    int AnalysisBlobTile(const unsigned char *ndsData, uint32_t nBytes, int coordWidth);

    int AnalysisBlobAuxTile(const unsigned char * ndsData,uint32_t nBytes);

    ~Widget();

    private
    slots:
    void on_TileIdList_itemClicked(QListWidgetItem *item);

    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_addBtn_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_typeComboBox_currentTextChanged(const QString &arg1);

private:
    Ui::Widget *ui;
    Display *display = nullptr;
    AddSpeedLimitToLink *speedLimitToLink = nullptr;

    NdsCommon * m_pNdsCommon = nullptr;

    QString selItem = "";

    CppSQLite3::Query m_query;
    int m_iBuildingBlockId = 0;
    int m_iLinkId = 0;
    int m_iTileId = 0;
    int m_iAttrSpeedLimitVal = 0;
    bool m_bIsNewTypeRef = false;


    QString m_strAttrTypeCode = "";
    int m_iRefType = -1;

    nds::routing::main::RoutingTile *m_pRoutingTile = nullptr;
    nds::common::flexattr::attrmaps::AttributeLayer * m_pRoutingAuxTile = nullptr;
    CppSQLite3::Database *m_pDBins = nullptr;

    vector<int> m_vecAllId;
    map<int, const unsigned char *> m_mNdsData;
    map<int, uint32_t> m_mNdsDataLen;

    AddParkingToLink * m_pAddParkingToLink = nullptr;
    AddProhibitedPassageToLink * m_pAddProhibitedPassageToLink = nullptr;
    AddStartOrDestinationRoadOnly * m_pAddStartOrDestinationRoadOnly = nullptr;
    AddRoadZLevel * m_pAddRoadZLevel = nullptr;
    AddFloorNumberToLink * m_pAddFloorNumber = nullptr;
    AddSpeedLimitToLink * m_pAddSpeedLimit = nullptr;
};

#endif // WIDGET_H
