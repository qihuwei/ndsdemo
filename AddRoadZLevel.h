#ifndef ADDROADZLEVEL_H
#define ADDROADZLEVEL_H

#include <QWidget>
#include <QButtonGroup>
#include <QMessageBox>
#include "NdsCommon.h"

namespace Ui {
class AddRoadZLevel;
}

class AddRoadZLevel : public QWidget
{
    Q_OBJECT

public:
    explicit AddRoadZLevel(QWidget *parent = nullptr);
    ~AddRoadZLevel();

    void    SetContext(NdsCommon * pNdsCommon);
    void    DoWork(int tileId, int linkId,int refType);
     nds::common::flexattr::attrmaps::AttrValueList &BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef);

private:
    Ui::AddRoadZLevel *ui;

    nds::common::flexattr::attrmaps::AttributeLayer *   m_pRoutingAuxTile =nullptr ;
    nds::routing::main::RoutingTile *                   m_pRoutingTile = nullptr;

    NdsCommon *         m_pNdsCommon = nullptr ;
    QButtonGroup *      m_pGroupBtn = nullptr;


    int m_iTileId = 0;
    int m_iLinkId = 0;

private slots:
    void slotsRadioClick();
    void on_roadHeigthLevelSpinBox_valueChanged(int arg1);
    void on_startPosSpinBox_valueChanged(int arg1);
    void on_endPosSpinBox_valueChanged(int arg1);
};

#endif // ADDROADZLEVEL_H
