#include "NdsCommon.h"
#include <QDebug>

NdsCommon::NdsCommon(nds::routing::main::RoutingTile *pRoutingTile,
                     nds::common::flexattr::attrmaps::AttributeLayer *pRoutingAuxTile,
                     CppSQLite3::Database *pDBHandle)
{
    this->m_pRoutingTile = pRoutingTile;
    this->m_pRoutingAuxTile = pRoutingAuxTile;
    this->m_pDBIns = pDBHandle;
}

int NdsCommon::FindAttrMapEntryIdx(datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals,int refType)
{
    int count = 0;
    int numMaps = m_pAttrMapListInstance -> getNumMaps();
    ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeArray = m_pAttrMapListInstance -> getAttrTypeRef();
    for(int i = 0; i < numMaps; ++i)
    {
        if ((refAttrTypeArray.elementAt(i).getNumAttrCodes() == attrVals.getSize() && m_pAttrMapListInstance->getAttrTypeRef().elementAt(i).getReferenceType() == static_cast<nds::common::flexattr::attrmaps::ReferenceType::e_referencetype>(refType)))
        {
            for(int j = 0; j < attrVals.getSize(); ++j)
            {
                // find a appropriate refAttrType
                if(refAttrTypeArray.elementAt(i).getAttributeTypeCodes().elementAt(j).getValue() == attrVals.elementAt(j).getValue())
                {
                    count ++;
                }
                if(count == attrVals.getSize())
                {
                    return i;
                }
            }
        }
    }

    return -1;
}

int NdsCommon::FindVal4xxxFeatureEntryIdx(int nMap,ObjectArray< nds::common::flexattr::valuecodes::AttributeValue >&attrVal)
{
    nds::common::flexattr::attrmaps::AttributeMap & refAttrMap = m_pAttrMapListInstance->getAttrMap().elementAt(nMap);
    ObjectArray< nds::common::flexattr::valuecodes::AttributeValue > attrValArray;

    for (int i = 0; i < refAttrMap.getNumEntries(); i++)
    {
        attrValArray = refAttrMap.getValues4ManyFeatures().elementAt(i).getAttrValList().getValues();
        for (int j = 0; j < attrVal.getSize(); j++)
        {
            switch (m_pAttrMapListInstance->getAttrTypeRef().elementAt(nMap).getAttributeTypeCodes().elementAt(j).getValue()) {
            case nds::common::flexattr::valuecodes::AttributeTypeCode::PARKING:
            {
                if (attrValArray.getData()->getParking().getFacilityType().getValue() == attrVal.getData()->getParking().getFacilityType().getValue()&&
                        attrValArray.getData()->getParking().getInOutType().getValue() == attrVal.getData()->getParking().getInOutType().getValue())
                {
                    return i;
                }
                break;
            }

            case nds::common::flexattr::valuecodes::AttributeTypeCode::SPEED_LIMIT:
            {
                if (attrValArray.getData()->getSpeedLimit() == attrVal.getData()->getSpeedLimit())
                {
                    return i;
                }
                break;
            }

            case nds::common::flexattr::valuecodes::AttributeTypeCode::PROHIBITED_PASSAGE:
            {
                if (attrValArray.elementAt(1).getTimeRangeOfDay().getStartTime() == attrVal.elementAt(1).getTimeRangeOfDay().getStartTime()&&
                        attrValArray.elementAt(1).getTimeRangeOfDay().getEndTime() == attrVal.elementAt(1).getTimeRangeOfDay().getEndTime()&&
                        attrValArray.elementAt(1).getTimeRangeOfDay().getIsInclusive() == attrVal.elementAt(1).getTimeRangeOfDay().getIsInclusive())
                {
                    return i;
                }
                break;
            }

            case nds::common::flexattr::valuecodes::AttributeTypeCode::ROAD_Z_LEVEL:
            {
                if(attrValArray.elementAt(0).getRoadHeightLevel() ==attrVal.elementAt(0).getRoadHeightLevel()&&
                        attrValArray.elementAt(1).getValidityRange().getStartPosition().getNumVx4() == attrVal.elementAt(1).getValidityRange().getStartPosition().getNumVx4()&&
                        attrValArray.elementAt(1).getValidityRange().getEndPosition().getNumVx4() == attrVal.elementAt(1).getValidityRange().getEndPosition().getNumVx4())
                {
                    return i;
                }

                break;
            }
            case nds::common::flexattr::valuecodes::AttributeTypeCode::START_OR_DESTINATION_ROAD_ONLY:
            {
                return i;
            }
            case nds::common::flexattr::valuecodes::AttributeTypeCode::FLOOR_NUMBER:
            {
                if (attrValArray.getData()->getFloorNumber() == attrVal.getData()->getFloorNumber())
                {
                    return i;
                }
                break;
            }
            }

        }
    }
    return -1;
}

nds::routing::main::RoutingTile * NdsCommon::GetRoutingTile()
{
    return  m_pRoutingTile;
}

nds::common::flexattr::attrmaps::AttributeLayer * NdsCommon::GetRoutingAuxTile()
{
    return m_pRoutingAuxTile;
}

void NdsCommon::BuildAttrVals4ManyFeature(nds::common::flexattr::attrmaps::AttrVals4ManyFeatures &entry,
                                          nds::common::flexattr::attrmaps::AttributeMap &refAttrMap, int nMap,
                                          nds::common::flexattr::attrmaps::FeatureReference &feature,
                                          datascript::ObjectArray <nds::common::flexattr::valuecodes::AttributeValue> &attrVals)
{
    ObjectArray <nds::common::flexattr::attrmaps::AttrVals4ManyFeatures> &entryVals4MantFArray = refAttrMap.getValues4ManyFeatures();

    datascript::ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> featureArray;
    featureArray.add(feature);
    entry.setNumFeatures(static_cast<uint16_t>(featureArray.getSize()));
    entry.setFeature(featureArray);

    nds::common::flexattr::attrmaps::AttrValueList attrValList;
    attrValList.setAttrRefHeader(m_pAttrMapListInstance -> getAttrTypeRef().elementAt(nMap));
    attrValList.setValues(attrVals);

    entry.setAttrValList(attrValList);
    entryVals4MantFArray.add(entry);
    refAttrMap.setNumEntries(static_cast<uint16_t>(entryVals4MantFArray.getSize()));
    refAttrMap.setValues4ManyFeatures(entryVals4MantFArray);
}

nds::common::flexattr::attrmaps::FeatureReference& NdsCommon::BuildFeature(int linkId, nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef)
{
    nds::common::flexattr::attrmaps::FeatureReference * feature = new nds::common::flexattr::attrmaps::FeatureReference;
    feature->setLinkId(static_cast<nds::common::LinkId>(linkId));
    switch(entryAttrTypeRef.getReferenceType().getValue())
    {
    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_SIMPLE_INTERSECTION:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_LINK_DIRECTED:
    {
        nds::common::DirectedLinkReference direLinkRef;
        direLinkRef.setPositiveLinkDirection(true);
        feature->setDirectedLinkRef(direLinkRef);
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_LINK_BOTH_DIRECTIONS:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_ROAD_GEO_LINE_DIRECTED:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_ROAD_GEO_LINE_BOTH_DIRECTIONS:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::ROUTING_SIMPLE_INTERSECTION_TRANSITION:
    {
        nds::common::flexattr::attrmaps::TransitionReferenceSimpleIntersection transitionReferenceSimpleIntersection;
        transitionReferenceSimpleIntersection.setSimpleIntersectionId(10);
        transitionReferenceSimpleIntersection.setTransitionOrdinalNumber(20);
        feature->setTransitionReferenceSimpleIntersections(transitionReferenceSimpleIntersection);
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::BMD_AREA_FEATURE:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::BMD_LINE_FEATURE_DIRECTED:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::BMD_LINE_FEATURE_BOTH_DIRECTIONS:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::BMD_POINT_FEATURE:
    {
        break;
    }
    case nds::common::flexattr::attrmaps::ReferenceType::ALL_TILE_FEATURES:
    {
        break;
    }
    }

    feature->setReferenceType(entryAttrTypeRef.getReferenceType());
    return *feature;
}

bool NdsCommon::AddEntry(int tileId,int nMap,
                         datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals,
                         nds::common::flexattr::attrmaps::FeatureReference &feature,
                         nds::common::flexattr::attrmaps::AttrValueList &attrValList)
{
    nds::common::flexattr::attrmaps::AttributeMap &refAttrMap = m_pAttrMapListInstance-> getAttrMap().elementAt(nMap);
    uint8_t value = refAttrMap.getAttrMapType().getValue();

    if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE == value)
    {
        int bitsMap1 = refAttrMap.bitsizeOf();
        // 构建AttrVals4OneFeature
        nds::common::flexattr::attrmaps::AttrVals4OneFeature entry;
        BuildAttrVals4OneFeature(tileId,entry,attrVals);

        // numEntries加1
        int numEntries = refAttrMap.getNumEntries();
        ++numEntries;
        refAttrMap.setNumEntries(static_cast<uint16_t>(numEntries));

        // 添加Entry
        refAttrMap.getValues4OneFeature().add(entry);

        int bitsMap2 = refAttrMap.bitsizeOf();

        // 修改索引号在nMap之后的每个AttributeMap所对应的AttributeTypeRef中的offset字段
        int bitsEntry = bitsMap2 - bitsMap1;
        ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pAttrMapListInstance -> getAttrTypeRef();
        nds::common::RelativeBlobOffset offset = 0;
        int numMaps = m_pAttrMapListInstance -> getNumMaps();
        for (int i = nMap + 1; i < numMaps; ++i) {
            nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
            offset = refAttrTypeRef.getAttrTypeOffset();
            offset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
            refAttrTypeRef.setAttrTypeOffset(offset);
        }
    }
    else if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES == value)
    {
        int bitsMap1 = refAttrMap.bitsizeOf();
        ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pAttrMapListInstance -> getAttrTypeRef();

        // bulid one feature
        nds::common::flexattr::attrmaps::AttrVals4ManyFeatures entry4ManyF;
        BuildAttrVals4ManyFeature(entry4ManyF,refAttrMap,nMap,feature,attrValList.getValues());

        int bitsMap2 = refAttrMap.bitsizeOf();
        int bitsEntry = bitsMap2 - bitsMap1;
        nds::common::RelativeBlobOffset offset = 0;
        int numMaps = m_pAttrMapListInstance -> getNumMaps();
        for (int i = nMap + 1; i < numMaps; ++i) {
            nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
            offset = refAttrTypeRef.getAttrTypeOffset();
            offset += (static_cast<uint32_t>((bitsEntry / 8)) + ((bitsEntry % 8) > 0 ? 1 : 0));
            refAttrTypeRef.setAttrTypeOffset(offset);
        }
    }
    return true;
}

void NdsCommon::BuildAttrVals4OneFeature(int tileId,nds::common::flexattr::attrmaps::AttrVals4OneFeature &entry,
                                         datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals)
{
    nds::common::flexattr::attrmaps::FeatureReference feature;
    nds::common::LinkId linkRef = static_cast<nds::common::LinkId>(tileId);
    feature.setLinkId(linkRef);
    entry.setFeature(feature);

    ObjectArray <nds::common::flexattr::valuecodes::AttributeValue> &refAttrValueArray = entry.getAttrValList().getValues();
    refAttrValueArray.clear();
    for(int i = 0; i < attrVals.getSize(); i++)
    {
        nds::common::flexattr::valuecodes::AttributeTypeCode attrType(attrVals.elementAt(i));
        nds::common::flexattr::valuecodes::AttributeValue attrValue;
        attrValue.setAttrType(attrType);
        refAttrValueArray.add(attrValue);
    }
}

void NdsCommon::BuildAttrTypeRef(nds::common::flexattr::attrmaps::AttributeTypeRef & entryAttrTypeRef,
                                 datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> &attrVals)
{
    entryAttrTypeRef.setNumAttrCodes(static_cast<uint8_t>(attrVals.getSize()));
    datascript::ObjectArray <nds::common::flexattr::valuecodes::AttributeTypeCode> refAttrTypeCodeArray;
    refAttrTypeCodeArray.clear();
    for(int i = 0; i < attrVals.getSize(); i++)
    {
        nds::common::flexattr::valuecodes::AttributeTypeCode attrTypeCode1(attrVals.elementAt(i));
        refAttrTypeCodeArray.add(attrTypeCode1);
    }

    entryAttrTypeRef.setAttributeTypeCodes(refAttrTypeCodeArray);
    nds::common::flexattr::attrmaps::ReferenceType referenceType(nds::common::flexattr::attrmaps::ReferenceType::e_referencetype::ROUTING_LINK_BOTH_DIRECTIONS);
    entryAttrTypeRef.setReferenceType(referenceType);
    entryAttrTypeRef.setAttrTypeOffset(0);
}

void NdsCommon::BuildAttrMap(nds::common::flexattr::attrmaps::AttributeMap &entryAttrMap,
                             nds::common::flexattr::attrmaps::AttrMapType & attrMapType,
                             nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef)
{
    entryAttrMap.setAttrMapType(attrMapType);
    entryAttrMap.setAttrRefHeader(entryAttrTypeRef);

    if(attrMapType.getValue() == nds::common::flexattr::attrmaps::AttrMapType::e_attrmaptype::VALUES_TO_ONE_FEATURE)
    {
        datascript::ObjectArray<nds::common::flexattr::attrmaps::AttrVals4OneFeature>  attrVals4OneFeatureArray;
        attrVals4OneFeatureArray.clear();

        nds::common::flexattr::attrmaps::AttrVals4OneFeature attrVals4OneFeature;
        attrVals4OneFeature.setAttrRefHeader(entryAttrTypeRef);

        attrVals4OneFeatureArray.add(attrVals4OneFeature);
        entryAttrMap.setNumEntries(static_cast<uint16_t>(attrVals4OneFeatureArray.getSize()));
        entryAttrMap.setValues4OneFeature(attrVals4OneFeatureArray);
    }
    else if(attrMapType.getValue() == nds::common::flexattr::attrmaps::AttrMapType::e_attrmaptype::VALUES_TO_MANY_FEATURES)
    {
        datascript::ObjectArray <nds::common::flexattr::attrmaps::AttrVals4ManyFeatures> attrVals4ManyFeatureArray;
        attrVals4ManyFeatureArray.clear();
        nds::common::flexattr::attrmaps::AttrVals4ManyFeatures attrVals4ManyFeatures;
        attrVals4ManyFeatures.setAttrRefHeader(entryAttrTypeRef);
        attrVals4ManyFeatures.setNumFeatures(1);
        attrVals4ManyFeatureArray.add(attrVals4ManyFeatures);
        entryAttrMap.setNumEntries(static_cast<uint16_t>(attrVals4ManyFeatureArray.getSize()));
        entryAttrMap.setValues4ManyFeatures(attrVals4ManyFeatureArray);
    }
    else
    {

    }
}

void NdsCommon::SetAttrMapListInstance(nds::common::flexattr::attrmaps::AttributeMapList *pAttrMapListInstance)
{
    if (pAttrMapListInstance != nullptr)
    {
        m_pAttrMapListInstance = pAttrMapListInstance;
    }
}

int NdsCommon::AddFeature(int nMap,int insIdx, int nEntry, int linkId)
{
    nds::common::flexattr::attrmaps::AttributeMap & refAttrMap = m_pAttrMapListInstance->getAttrMap().elementAt(nMap);
    ObjectArray< nds::common::flexattr::attrmaps::AttributeTypeRef >& refAttrTypeRefArray = m_pAttrMapListInstance -> getAttrTypeRef();
    uint8_t value = refAttrMap.getAttrMapType().getValue();

    if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE == value)
    {
        int bitsMap1 = refAttrMap.bitsizeOf();
        int numEntries = refAttrMap.getNumEntries();
        ObjectArray <nds::common::flexattr::attrmaps::AttrVals4ManyFeatures> entry4ManyFArray;
        entry4ManyFArray.clear();

        ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> featureArray;
        nds::common::flexattr::attrmaps::AttrVals4ManyFeatures entry4ManyF;
        nds::common::flexattr::attrmaps::FeatureReference feature;

        for (int i = 0; i < numEntries; ++i)
        {
            nds::common::flexattr::attrmaps::AttrVals4OneFeature &refEntry4OneF = refAttrMap.getValues4OneFeature().elementAt(i);

            if (i != nEntry)
            {
                entry4ManyF.setNumFeatures(1);
            }
            else
            {
                entry4ManyF.setNumFeatures(2);
            }

            nds::common::flexattr::attrmaps::FeatureReference &refFeature = refEntry4OneF.getFeature();
            featureArray.add(refFeature);
            if (i == nEntry)
            {
                nds::common::flexattr::attrmaps::FeatureReference feature = BuildFeature(linkId,refAttrTypeRefArray.elementAt(nMap));
                featureArray.add(feature);
            }
            entry4ManyF.setFeature(featureArray);
            nds::common::flexattr::attrmaps::AttrValueList &refAttrValueList = refEntry4OneF.getAttrValList();
            entry4ManyF.setAttrValList(refAttrValueList);
            entry4ManyFArray.add(entry4ManyF);
        }

        refAttrMap.setAttrMapType(nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES);

        refAttrMap.setValues4ManyFeatures(entry4ManyFArray);
        int bitsMap2 = refAttrMap.bitsizeOf();
        // 修改索引号在nMap之后的每个AttributeMap所对应的AttributeTypeRef中的offset字段
        int bitsEntry = bitsMap2 - bitsMap1;
        ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pAttrMapListInstance -> getAttrTypeRef();
        nds::common::RelativeBlobOffset offset = 0;
        int numMaps = m_pAttrMapListInstance -> getNumMaps();
        for (int i = nMap + 1; i < numMaps; ++i)
        {
            nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
            offset = refAttrTypeRef.getAttrTypeOffset();
            offset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
            refAttrTypeRef.setAttrTypeOffset(offset);
        }
        // 修改header.externalTileIdListOffset的值
        //        nds::common::RelativeBlobOffset externalTileIdListOffset = m_pRoutingAuxTile->getHeader().getExternalTileIdListOffset();
        //        externalTileIdListOffset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
        //        m_pRoutingTile->getHeader().setExternalTileIdListOffset(externalTileIdListOffset);
    }
    else if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES == value)
    {
        int bitsMap1 = refAttrMap.bitsizeOf();
        datascript::ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> &featureArray = refAttrMap.getValues4ManyFeatures().elementAt(insIdx).getFeature();
        nds::common::flexattr::attrmaps::FeatureReference feature =  BuildFeature(linkId,refAttrTypeRefArray.elementAt(nMap));
        featureArray.add(feature);
        refAttrMap.getValues4ManyFeatures().elementAt(insIdx).setFeature(featureArray);
        refAttrMap.getValues4ManyFeatures().elementAt(insIdx).setNumFeatures(static_cast<uint16_t>(featureArray.getSize()));

        int bitsMap2 = refAttrMap.bitsizeOf();
        int bitsEntry = bitsMap2 - bitsMap1;
        ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pAttrMapListInstance -> getAttrTypeRef();
        nds::common::RelativeBlobOffset offset = 0;
        int numMaps = m_pAttrMapListInstance -> getNumMaps();
        for (int i = nMap + 1; i < numMaps; ++i)
        {
            nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
            offset = refAttrTypeRef.getAttrTypeOffset();
            offset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
            refAttrTypeRef.setAttrTypeOffset(offset);
        }
    }

    return 0;
}

bool NdsCommon::Update2Database(QString dbName, QString filedName, int tileId)
{
    int nBits =0,nBytes = 0;
    if (0 == dbName.toStdString().compare("routingTileTable"))
    {
        //  get new blob
        nBits = m_pRoutingTile -> bitsizeOf();
    }
    else if (0 == dbName.toStdString().compare("routingAuxTileTable"))
    {
        nBits = m_pRoutingAuxTile -> bitsizeOf();
    }
    nBytes = nBits / 8 + ((nBits % 8) > 0 ? 1 : 0);
    uint8_t *writeStream = new uint8_t[nBytes];
    datascript::ExtParams extParam;
    datascript::BitStreamWriter bsw(writeStream, static_cast<uint32_t>(nBytes));

    if (0 == dbName.toStdString().compare("routingTileTable"))
    {
        m_pRoutingTile -> write(bsw, extParam);
    }
    else if (0 == dbName.toStdString().compare("routingAuxTileTable"))
    {
        m_pRoutingAuxTile -> write(bsw, extParam);
    }

    unsigned char *pBlob = new unsigned char[nBytes + 1];
    memset(pBlob, 0, static_cast<size_t>(nBytes + 1));
    for (int i = 0; i < nBytes; ++i)
    {
        pBlob[i] = static_cast<unsigned char>(writeStream[i]);
    }

    delete[] writeStream;

    int nRet = -1;
    try
    {
        // 在数据库中更新BLOB
        char buf[128];
        memset(buf, 0, sizeof(buf));
        sprintf(buf, "UPDATE %s SET %s = ?1 WHERE Id = %d;",dbName.toStdString().c_str(),filedName.toStdString().c_str(), tileId);

        CppSQLite3::Statement updTile = m_pDBIns->compileStatement(buf);
        updTile.bindBlob(1, pBlob, nBytes);
        nRet = updTile.execDML();
    }
    catch (...)
    {
        delete[] pBlob;
        return false;
    }

    delete[] pBlob;

    if (nRet == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}
