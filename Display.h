#ifndef DISPLAY_H
#define DISPLAY_H

#include <QTreeWidgetItem>
#include "ui_widget.h"

#include <QWidget>
#include "CppSQLite3/Database.h"
#include "CppSQLite3/Query.h"
#include "datascript/DbAccessMode.h"
#include "datascript/Util.h"

#ifndef DATASCRIPT_RUNTIME_INCLUDE_VALIDATION
#define DATASCRIPT_RUNTIME_INCLUDE_VALIDATION
#endif

#include "nds/routing/main/RoutingTile.h"
#include "nds/routing/main/RoutingTileTable.h"
#include "nds/routing/main/RoutingGeoTile.h"
#include "nds/all/ExtParams.h"
#include <nds/common/flexattr/attrmaps/AttributeLayer.h>

class Display : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief   构造函数，初始化成员属性
     * @param
     * @return
     */
    Display(QWidget *parent = nullptr, Ui::Widget *ui = nullptr,
            nds::routing::main::RoutingTile *pRoutingTile = nullptr,
            nds::common::flexattr::attrmaps::AttributeLayer * pRoutingAuxTile = nullptr);

    /**
     * @brief   向 TreeWidget 上添加一个 item
     * @param   parent      TreeWidget 的父节点
     * @param   name        item's name
     * @param   value       item's value
     * @return  添加的 item 的地址
     */
    QTreeWidgetItem *QTreeWidget_addItem(QTreeWidgetItem *parent, QString name, QString value = "");

    /**
     * @brief   向 TreeWidget 上添加一个根 item
     * @param   name        item's name
     * @return  根 item 的地址
     */
    QTreeWidgetItem *QTreeWidget_addParentItem(QString name);

    /**
     * @brief   display TreeWidget
     * @param
     * @return
     */
    void ShowRoutingTile();

    void ShowRoutingAuxTile();

    /**
     * @brief   nalysis FixedRoadAttributeSetList
     * @param
     * @return
     */
    void AnalysisFixedRoadAttributeSetList();

    /**
     * @brief   analysis SimpleIntersectionList
     * @param
     * @return
     */
    void AnalysisSimpleIntersectionList();

    /**
     * @brief   analysis LinkList
     * @param
     * @return
     */
    void AnalysisLinkList();

    /**
     * @brief   analysis RouteUpLinkList
     * @param
     * @return
     */
    void AnalysisRouteUpLinkList();

    /**
     * @brief   analysis RouteDownLinkList
     * @param
     * @return
     */
    void AnalysisRouteDownLinkList();

    /**
     * @brief   analysis AttributeMapList
     * @param
     * @return
     */
    void AnalysisAttributeMapList();

    /**
     * @brief   analysis ExternalTileIdList
     * @param
     * @return
     */

    void SetAttrMapListInstance( nds::common::flexattr::attrmaps::AttributeMapList * pAttrMapListInstance);
    void AnalysisExternalTileIdList();

signals:

private:
    Ui::Widget *ui;
    QTreeWidgetItem *                                       m_pAttributeMapItem = nullptr;
    nds::routing::main::RoutingTile *                       m_pRoutingTile = nullptr;
    nds::common::flexattr::attrmaps::AttributeLayer *       m_pRoutingAuxTile = nullptr;
    nds::common::flexattr::attrmaps::AttributeMapList *     m_pAttrMapListInstance;

public slots:

private slots:

};

#endif // DISPLAY_H
