#include "AddRoadZLevel.h"
#include "ui_AddRoadZLevel.h"

static int roadHeightLevel = 0;
static int startPosition = 0;
static int endPosition = 0;

AddRoadZLevel::AddRoadZLevel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddRoadZLevel)
{
    ui->setupUi(this);
    m_pGroupBtn = new QButtonGroup(this);
    m_pGroupBtn->addButton(ui->routingAuxTileRadioBtn,0);


    // binding Signal
    connect(ui->routingAuxTileRadioBtn,SIGNAL(clicked()),this,SLOT(slotsRadioClick()));


    SelTableName = "RoutingAuxTileTable";

}

AddRoadZLevel::~AddRoadZLevel()
{
    delete ui;
}

void AddRoadZLevel::SetContext(NdsCommon *pNdsCommon)
{
    this->m_pNdsCommon = pNdsCommon;

    m_pRoutingTile = m_pNdsCommon->GetRoutingTile();
    m_pRoutingAuxTile = m_pNdsCommon->GetRoutingAuxTile();
}

void AddRoadZLevel::DoWork(int tileId, int linkId, int refType)
{
    m_iTileId = tileId;
    m_iLinkId = linkId;
    int attrTypeRefBitEntry = 0;
    int attrMapBitEntry = 0;
    int bitsEntry = 0;
    int tempNmap = 0;
    int nMap;
    bool updateRet = false;

    nds::common::flexattr::valuecodes::AttributeTypeCode attrTypeCode1 = nds::common::flexattr::valuecodes::AttributeTypeCode::ROAD_Z_LEVEL;
    nds::common::flexattr::valuecodes::AttributeTypeCode attrTypeCode2 = nds::common::flexattr::valuecodes::AttributeTypeCode::VALIDITY_RANGE;
    datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> attrVals;
    attrVals.add(attrTypeCode1);
    attrVals.add(attrTypeCode2);
    std::cout << "##### = " << selTableId << std::endl;

    switch (selTableId) {
        case 0:
        {
            m_pNdsCommon->SetAttrMapListInstance(&(m_pRoutingAuxTile->getAttributeMapList()));
            nMap = m_pNdsCommon -> FindAttrMapEntryIdx(attrVals,refType);

            if (nMap == -1)
            {
                // 模拟新加一个 attrTypeRef 和 attrMap
                tempNmap = m_pRoutingAuxTile -> getAttributeMapList().getNumMaps();
                nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef;
                m_pNdsCommon -> BuildAttrTypeRef(entryAttrTypeRef,attrVals);
                m_pRoutingAuxTile -> getAttributeMapList().getAttrTypeRef().add(entryAttrTypeRef);
                // build AttributeMap
                nds::common::flexattr::attrmaps::AttributeMap entryAttrMap;
                nds::common::flexattr::attrmaps::AttrMapType attrMapType = nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES;
                m_pNdsCommon -> BuildAttrMap( entryAttrMap, attrMapType, entryAttrTypeRef);
                m_pRoutingAuxTile -> getAttributeMapList().setNumMaps(static_cast<uint16_t>(tempNmap + 1));

                attrTypeRefBitEntry = m_pRoutingAuxTile -> getAttributeMapList().getAttrTypeRef().elementAt(tempNmap).bitsizeOf();
                attrMapBitEntry = m_pRoutingAuxTile -> getAttributeMapList().getAttrMap().elementAt(tempNmap).bitsizeOf();
                bitsEntry = attrTypeRefBitEntry + attrMapBitEntry;

                // 更新新添加的 attrTypeRef 的偏移量
                nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = m_pRoutingAuxTile -> getAttributeMapList().getAttrTypeRef().elementAt(tempNmap);
                nds::common::RelativeBlobOffset offset = m_pRoutingAuxTile -> getAttributeMapList().getAttrTypeRef().elementAt(tempNmap - 1).getAttrTypeOffset();
                offset += (static_cast<uint32_t>((bitsEntry / 8)) + ((bitsEntry % 8) > 0 ? 1 : 0));
                refAttrTypeRef.setAttrTypeOffset(offset);
            }
            else
            {
                nds::common::flexattr::attrmaps::AttrValueList attrValList = BuildAttrValList(m_pRoutingAuxTile->getAttributeMapList().getAttrTypeRef().elementAt(nMap));
                int vals4xxxFeaIdx = m_pNdsCommon->FindVal4xxxFeatureEntryIdx(nMap,attrValList.getValues());
                if(vals4xxxFeaIdx != -1)
                {
                    int addRet = m_pNdsCommon -> AddFeature(nMap,vals4xxxFeaIdx, 1, this->m_iLinkId);
                    if (addRet != 0) {
                        QMessageBox::information(this, "AddResult", "Add Feature Failed!");
                    }
                }

                // 没有找到合适的 attrVal ,需要新建一个 values4OneFeatures
                else
                {
                    nds::common::flexattr::attrmaps::FeatureReference feature = m_pNdsCommon->BuildFeature(m_iLinkId,m_pRoutingAuxTile->getAttributeMapList().getAttrTypeRef().elementAt(nMap));

                    if (m_pNdsCommon -> AddEntry(m_iTileId,nMap,attrVals,feature,attrValList))
                    {
                        QMessageBox::information(this, "AddResult", "Add Entry Successful!");
                    }
                    else
                    {
                        QMessageBox::critical(this, "AddResult", "Add Entry Failed!");
                    }
                }
            }

            updateRet =  m_pNdsCommon -> Update2Database(SelTableName,"guidanceAttributeLayer",tileId);
            break;
        }

    }
    if (updateRet)
        QMessageBox::information(this,"Update Database result","Update Database Successful!");
}

nds::common::flexattr::attrmaps::AttrValueList &AddRoadZLevel::BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef)
{
    // build one attrValList entry
    datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeValue> attrVals;
    nds::common::flexattr::valuecodes::AttributeValue attrVal1;
    nds::common::flexattr::valuecodes::AttributeValue attrVal2;
    attrVal1.setAttrType(nds::common::flexattr::valuecodes::AttributeTypeCode::ROAD_Z_LEVEL);
    attrVal1.setRoadHeightLevel(static_cast<int8_t>(roadHeightLevel));
    attrVal2.setAttrType(nds::common::flexattr::valuecodes::AttributeTypeCode::VALIDITY_RANGE);

    nds::common::ValidityRange validityRange;
    nds::common::geometry::LinePosition startLinePos;
    startLinePos.setNumVx4(static_cast<uint8_t>(startPosition));
    validityRange.setStartPosition(startLinePos);
    nds::common::geometry::LinePosition endLinePos;
    endLinePos.setNumVx4(static_cast<uint8_t>(endPosition));
    validityRange.setEndPosition(startLinePos);
    validityRange.setEndPosition(endLinePos);
    attrVal2.setValidityRange(validityRange);

    attrVals.add(attrVal1);
    attrVals.add(attrVal2);

    nds::common::flexattr::attrmaps::AttrValueList *attrValList = new nds::common::flexattr::attrmaps::AttrValueList;
    attrValList -> setValues(attrVals);
    attrValList->setAttrRefHeader(entryAttrTypeRef);

    return *attrValList;
}

void AddRoadZLevel::slotsRadioClick()
{
    selTableId = m_pGroupBtn->checkedId();

    switch (m_pGroupBtn->checkedId()) {
        case 0:
        {
            SelTableName = "routingAuxTileTable";
            break;
        }
    }
}

void AddRoadZLevel::on_roadHeigthLevelSpinBox_valueChanged(int arg1)
{
    roadHeightLevel = arg1;
}

void AddRoadZLevel::on_startPosSpinBox_valueChanged(int arg1)
{
    startPosition = arg1;
}

void AddRoadZLevel::on_endPosSpinBox_valueChanged(int arg1)
{
    endPosition = arg1;
}
