#ifndef ADDSPEEDLIMITTOLINK
#define ADDSPEEDLIMITTOLINK

#include <QWidget>
#include <QButtonGroup>
#include <QMessageBox>
#include "NdsCommon.h"

namespace Ui {
class AddSpeedLimitToLink;
}

class AddSpeedLimitToLink : public QWidget
{
    Q_OBJECT

public:
    explicit AddSpeedLimitToLink(QWidget *parent = nullptr);
    ~AddSpeedLimitToLink();

    void    SetContext(NdsCommon * pNdsCommon);
    void    DoWork(int tileId, int linkId,int refType);
     nds::common::flexattr::attrmaps::AttrValueList &BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef);

private:
    Ui::AddSpeedLimitToLink *ui;

    nds::common::flexattr::attrmaps::AttributeLayer *   m_pRoutingAuxTile =nullptr ;
    nds::routing::main::RoutingTile *                   m_pRoutingTile = nullptr;

    NdsCommon *         m_pNdsCommon = nullptr ;
    QButtonGroup *      m_pGroupBtn = nullptr;


    int m_iTileId = 0;
    int m_iLinkId = 0;

private slots:
   void slotsRadioClick();
   void on_valueSpinBox_valueChanged(int arg1);
};

#endif // ADDSPEEDLIMITTOLINK
