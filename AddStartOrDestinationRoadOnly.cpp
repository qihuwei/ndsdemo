#include "AddStartOrDestinationRoadOnly.h"
#include "ui_AddStartOrDestinationRoadOnly.h"



AddStartOrDestinationRoadOnly::AddStartOrDestinationRoadOnly(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddStartOrDestinationRoadOnly)
{
    ui->setupUi(this);

    // bind Signal
    connect(ui->routingTileRadioBtn,SIGNAL(clicked()),this,SLOT(slotsRadioClick()));

    m_pGroupBtn = new QButtonGroup(this);
    m_pGroupBtn->addButton(ui->routingTileRadioBtn,0);

    SelTableName = "RoutingTileTable";
}

AddStartOrDestinationRoadOnly::~AddStartOrDestinationRoadOnly()
{
    delete ui;
}

void AddStartOrDestinationRoadOnly::SetContext(NdsCommon *pNdsCommon)
{
    this->m_pNdsCommon = pNdsCommon;

    m_pRoutingTile = m_pNdsCommon->GetRoutingTile();
    m_pRoutingAuxTile = m_pNdsCommon->GetRoutingAuxTile();
}

void AddStartOrDestinationRoadOnly::DoWork(int tileId, int linkId, int refType)
{
    m_iTileId = tileId;
    m_iLinkId = linkId;
    int attrTypeRefBitEntry = 0;
    int attrMapBitEntry = 0;
    int bitsEntry = 0;
    int tempNmap = 0;
    int nMap;
    bool updateRet = false;

    nds::common::flexattr::valuecodes::AttributeTypeCode attrTypeCode = nds::common::flexattr::valuecodes::AttributeTypeCode::START_OR_DESTINATION_ROAD_ONLY;
    datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> attrVals;
    attrVals.add(attrTypeCode);

    switch (selTableId) {
        // m_pRoutingTile
        case 0:
        {
            m_pNdsCommon->SetAttrMapListInstance(&(m_pRoutingTile->getAttributeMaps()));
            nMap = m_pNdsCommon -> FindAttrMapEntryIdx(attrVals,refType);

            if (nMap == -1)
            {
                // 模拟新加一个 attrTypeRef 和 attrMap
                tempNmap = m_pRoutingTile -> getAttributeMaps().getNumMaps();
                nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef;
                m_pNdsCommon -> BuildAttrTypeRef(entryAttrTypeRef,attrVals);
                m_pRoutingTile -> getAttributeMaps().getAttrTypeRef().add(entryAttrTypeRef);
                // build AttributeMap
                nds::common::flexattr::attrmaps::AttributeMap entryAttrMap;
                nds::common::flexattr::attrmaps::AttrMapType attrMapType = nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES;
                m_pNdsCommon -> BuildAttrMap( entryAttrMap, attrMapType, entryAttrTypeRef);
                m_pRoutingTile -> getAttributeMaps().setNumMaps(static_cast<uint16_t>(tempNmap + 1));

                attrTypeRefBitEntry = m_pRoutingTile -> getAttributeMaps().getAttrTypeRef().elementAt(tempNmap).bitsizeOf();
                attrMapBitEntry = m_pRoutingTile -> getAttributeMaps().getAttrMap().elementAt(tempNmap).bitsizeOf();
                bitsEntry = attrTypeRefBitEntry + attrMapBitEntry;

                // 更新新添加的 attrTypeRef 的偏移量
                nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = m_pRoutingTile -> getAttributeMaps().getAttrTypeRef().elementAt(tempNmap);
                nds::common::RelativeBlobOffset offset = m_pRoutingTile -> getAttributeMaps().getAttrTypeRef().elementAt(tempNmap - 1).getAttrTypeOffset();
                offset += (static_cast<uint32_t>((bitsEntry / 8)) + ((bitsEntry % 8) > 0 ? 1 : 0));
                refAttrTypeRef.setAttrTypeOffset(offset);
            }
            else
            {
                nds::common::flexattr::attrmaps::AttrValueList attrValList = BuildAttrValList(m_pRoutingTile->getAttributeMaps().getAttrTypeRef().elementAt(nMap));
                int vals4xxxFeaIdx = m_pNdsCommon->FindVal4xxxFeatureEntryIdx(nMap,attrValList.getValues());
                if(vals4xxxFeaIdx != -1)
                {
                    int addRet = m_pNdsCommon -> AddFeature(nMap,vals4xxxFeaIdx, 1, this->m_iLinkId);
                    if (addRet != 0) {
                        QMessageBox::information(this, "AddResult", "Add Feature Failed!");
                    }
                }

                // 没有找到合适的 attrVal ,需要新建一个 values4OneFeatures
                else
                {
                    nds::common::flexattr::attrmaps::FeatureReference feature = m_pNdsCommon->BuildFeature(m_iLinkId,m_pRoutingTile->getAttributeMaps().getAttrTypeRef().elementAt(nMap));

                    if (m_pNdsCommon -> AddEntry(m_iTileId,nMap,attrVals,feature,attrValList))
                    {
                        QMessageBox::information(this, "AddResult", "Add Entry Successful!");
                    }
                    else
                    {
                        QMessageBox::critical(this, "AddResult", "Add Entry Failed!");
                    }
                }
            }

            updateRet =  m_pNdsCommon -> Update2Database(SelTableName,"ndsData",tileId);
            break;
        }
    }
    if (updateRet)
        QMessageBox::information(this,"Update Database result","Update Database Successful!");
}

void AddStartOrDestinationRoadOnly::slotsRadioClick()
{
    selTableId = m_pGroupBtn->checkedId();

    switch (m_pGroupBtn->checkedId()) {
        case 0:
        {
            SelTableName = "routingTileTable";
            break;
        }
    }
}

nds::common::flexattr::attrmaps::AttrValueList &AddStartOrDestinationRoadOnly::BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef)
{
    // build one attrValList entry
    datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeValue> attrVals;
    nds::common::flexattr::valuecodes::AttributeValue attrVal1;
    attrVal1.setAttrType(nds::common::flexattr::valuecodes::AttributeTypeCode::START_OR_DESTINATION_ROAD_ONLY);
    attrVals.add(attrVal1);

    nds::common::flexattr::attrmaps::AttrValueList *attrValList = new nds::common::flexattr::attrmaps::AttrValueList;
    attrValList -> setValues(attrVals);
    attrValList->setAttrRefHeader(entryAttrTypeRef);

    return *attrValList;
}
