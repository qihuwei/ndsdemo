#ifndef ADDPARKINGTOLINK_H
#define ADDPARKINGTOLINK_H

#include <QWidget>
#include <QDebug>
#include <QButtonGroup>
#include <QMessageBox>



#ifndef DATASCRIPT_RUNTIME_INCLUDE_VALIDATION
#define DATASCRIPT_RUNTIME_INCLUDE_VALIDATION
#endif

#include <nds/common/flexattr/attrmaps/AttributeLayer.h>
#include "nds/routing/main/RoutingTile.h"
#include "nds/routing/main/RoutingTileTable.h"
#include "NdsCommon.h"

namespace Ui {
class AddParkingToLink;
}

class AddParkingToLink : public QWidget
{
    Q_OBJECT

public:
    AddParkingToLink(QWidget *parent = nullptr);

    /**
     * @brief   设置上下文
     * @param   NdsCommon 对象指针
     * @return
     */
    void    SetContext(NdsCommon * pNdsCommon);

    /**
     * @brief   AddParkingToLink 的操作业务逻辑
     * @param   tileId               tileId
     * @param   linkId               linkId
     * @param   refType              refType
     * @return
     */
    void    DoWork(int tileId, int linkId,int refType);

    /**
     * @brief   创建一个 attrValList 实例
     * @param   entryAttrTypeRef   一个AttrType的引用
     * @return
     */
    nds::common::flexattr::attrmaps::AttrValueList &BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef);

    ~AddParkingToLink();

private:
    Ui::AddParkingToLink *ui;

    nds::common::flexattr::attrmaps::AttributeLayer *   m_pRoutingAuxTile =nullptr ;
    nds::routing::main::RoutingTile *                   m_pRoutingTile = nullptr;

    NdsCommon *         m_pNdsCommon = nullptr ;
    QButtonGroup *      m_pGroupBtn = nullptr;
    QString             m_sFacilityType = "";
    QString             m_sInOutType = "";

    int m_iTileId = 0;
    int m_iLinkId = 0;

private slots:
    // RadioButton 的槽函数
    void slotsRadioClick();
    void on_facilityTypeComboBox_currentTextChanged(const QString &arg1);
    void on_inOutTypeComboBox_currentTextChanged(const QString &arg1);
};

#endif // ADDPARKINGTOLINK_H
