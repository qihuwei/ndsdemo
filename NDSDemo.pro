#-------------------------------------------------
#
# Project created by QtCreator 2019-07-09T16:44:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NDSDemo
TEMPLATE = app

INCLUDEPATH+=/data/release/Global/SqLitepp_20190703/x86_64_Ubuntu_16.04/include
INCLUDEPATH+=/data/release/Global/SqLite_latest/x86_64_Ubuntu_16.04/include
INCLUDEPATH+=/data/release/Global/SqLitepp_latest/x86_64_Ubuntu_18.04/include
INCLUDEPATH+=/data/release/Global/Common_19_10_20190906/x86_64_Ubuntu_16.04/include
INCLUDEPATH+=/data/release/Global/Boost_latest/x86_64_Ubuntu_16.04/include
INCLUDEPATH+=/data/id/release/NDS/NDS_CodeGen_2.5.4_20190704/x86_64_Ubuntu_16.04/nds_api/cpp/2_5_4/include

LIBS += -L/data/release/Global/SqLitepp_20190703/x86_64_Ubuntu_16.04/lib/ -lCppSQLite3
LIBS += -L/data/release/Global/SqLitepp_20190703/x86_64_Ubuntu_16.04/lib/ -lCppSQLite3Stubs
LIBS += -L/data/release/Global/SqLitepp_20190703/x86_64_Ubuntu_16.04/lib/ -lPermanentIds
LIBS += -L/data/release/Global/SqLiteToolkit_latest/x86_64_Ubuntu_16.04/lib/ -lSqLiteToolkit
LIBS += -L/data/id/release/NDS/NDS_CodeGen_2.5.4_20190704/x86_64_Ubuntu_16.04/nds_api/cpp/2_5_4/ -lnds_api_2_5_4
LIBS += -L/data/release/Global/Common_latest/x86_64_Ubuntu_16.04/lib/ -lCommon
LIBS += -L/data/release/Global/SqLite_latest/x86_64_Ubuntu_16.04/lib -lsqlite3f
LIBS += \
    -lpthread\
    -ldl


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
    Display.cpp \
    Widget.cpp \
    AddParkingToLink.cpp \
    AddProhibitedPassageToLink.cpp \
    NdsCommon.cpp \
    AddStartOrDestinationRoadOnly.cpp \
    AddRoadZLevel.cpp \
    AddFloorNumberToLink.cpp \
    AddSpeedLimitToLink.cpp

HEADERS += \
    Display.h \
    Widget.h \
    AddParkingToLink.h \
    AddProhibitedPassageToLink.h \
    NdsCommon.h \
    AddStartOrDestinationRoadOnly.h \
    AddRoadZLevel.h \
    AddFloorNumberToLink.h \
    AddSpeedLimitToLink.h

FORMS += \
        widget.ui \
    AddParkingToLink.ui \
    AddProhibitedPassageToLink.ui \
    AddStartOrDestinationRoadOnly.ui \
    AddRoadZLevel.ui \
    AddFloorNumberToLink.ui \
    AddSpeedLimitToLink.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
