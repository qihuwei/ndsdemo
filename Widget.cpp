#include "Widget.h"
#include "ui_widget.h"
#include "AddParkingToLink.h"

using namespace std;

#define product_db_file_path "/data/product-a/MIB2/Test_data/chenxi/20190612_GENNDS-3633_NUM_NORMAL_LANES_needed_in_high_level_routing/o_NDS-chn_nif_GENNDS_19Q1_GR11_1906_OneIDBG_20190612_GENNDS-3633_V1d3_MC_NDS/PRODUCT/PRODUCT.NDS"
#define overall_db_file_path "/data/product-a/MIB2/Test_data/chenxi/20190612_GENNDS-3633_NUM_NORMAL_LANES_needed_in_high_level_routing/o_NDS-chn_nif_GENNDS_19Q1_GR11_1906_OneIDBG_20190612_GENNDS-3633_V1d3_MC_NDS/PRODUCT/CN92/OVERALL.NDS"

//#define routing_db_file_path "/data/product-a/MIB2/Test_data/chenxi/20190612_GENNDS-3633_NUM_NORMAL_LANES_needed_in_high_level_routing/o_NDS-chn_nif_GENNDS_19Q1_GR11_1906_OneIDBG_20190612_GENNDS-3633_V1d3_MC_NDS/PRODUCT/CN92/ROUTING.NDS"
#define routing_db_file_path "/data/users/qihuwei/DataBase/PRODUCT/CN92/ROUTING.NDS"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->treeWidget->setColumnCount(2);
    ui->treeWidget->setColumnWidth(0, 400);

    m_iBuildingBlockId = GetBuildingBlockId("Routing");

    //ui->valueSpinBox->setEnabled(false);
    ui->typeComboBox->setEnabled(false);
    ui->refTypeComboBox->setEnabled(false);
    ui->addBtn->setEnabled(false);
    //ui->valueSpinBox->setValue(60);
    //m_iAttrSpeedLimitVal = ui->valueSpinBox->value();

    ui->parkingWidget->hide();
    ui->prohibitedPassageWidget->hide();
    ui->startOrDestinationRoadOnlyWidget->hide();
    ui->roadZLevelWidget->hide();
    ui->floorNumberWidget->hide();
    ui->speedLimitWidget->hide();
}

int Widget::GetBuildingBlockId(QString buildingBlockName)
{
    CppSQLite3::Database *db_handle = new CppSQLite3::Database();
    db_handle->open(product_db_file_path, SQLITE_OPEN_READWRITE);

    if (!db_handle->isOpen())
    {
        delete db_handle;
        std::cout << "Open PRODUCT.NDS Failed！" << std::endl;
        return -2;
    }
    std::string sql = QString("SELECT buildingBlockId FROM buildingBlockTable WHERE buildingBlockName = \"%1\"").arg(buildingBlockName).toStdString();
    CppSQLite3::Query query = db_handle->execQuery(sql);
    db_handle->close();
    delete db_handle;

    return query.getIntField("buildingBlockId");
}

int Widget::CoordWidth(int buildingBlockId, int tileId)
{
    int levelNumber = static_cast<int>(log((static_cast<double>(tileId))) / log(2.0) - 16);

    CppSQLite3::Database *db_handle = new CppSQLite3::Database();
    db_handle->open(overall_db_file_path, SQLITE_OPEN_READONLY);
    if (!db_handle->isOpen()) {
        delete db_handle;
        std::cout << "Open OVERALL.NDS Failed！" << std::endl;
        return -2;
    }

    std::string sql = QString("SELECT coordShift FROM levelMetadataTable WHERE levelNumber = %1 and buildingBlockId = %2").arg(levelNumber).arg(buildingBlockId).toStdString();
    CppSQLite3::Query query = db_handle->execQuery(sql);
    db_handle->close();
    delete db_handle;
    return 31 - levelNumber - query.getIntField("coordShift");
}

void Widget::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    QString text = item->text(0);
    const char *str = text.toStdString().c_str();
    if (strncmp("link[", str, 5) != 0)
    {
        //ui->valueSpinBox->setEnabled(false);
        ui->typeComboBox->setEnabled(false);
        ui->refTypeComboBox->setEnabled(false);
        ui->addBtn->setEnabled(false);
    }
    else
    {
        //ui->valueSpinBox->setEnabled(true);
        ui->typeComboBox->setEnabled(true);
        ui->refTypeComboBox->setEnabled(true);
        ui->addBtn->setEnabled(true);
        m_iLinkId = atoi(str + 5);

        m_strAttrTypeCode = ui->typeComboBox->currentText();
        m_iRefType = ui->refTypeComboBox->currentIndex();
        m_pAddSpeedLimit = new AddSpeedLimitToLink(this);
        ui->speedLimitWidget->show();

        // 实例化 SpeedLimitToLink
        //speedLimitToLink = new AddSpeedLimitToLink(this->m_pRoutingAuxTile, m_iTileId, m_pDBins, m_iLinkId, m_iAttrSpeedLimitVal);
    }
}

void Widget::InitDatabase()
{
    // get a database operator handle
    m_pDBins = new CppSQLite3::Database();

    // open database
    try {
        m_pDBins->open(routing_db_file_path, SQLITE_OPEN_READWRITE);
    }
    catch (exception e) {
        cout << "open routing_db_file_path Failed!" << endl;
        std::cout << e.what() << std::endl;
    }
}

int Widget::AnalysisBlobTile(const unsigned char *ndsData, uint32_t nBytes, int coordWidth)
{
    datascript::BitStreamReader bsr(ndsData, nBytes);
    bool validTile = true;
    datascript::ExtParams extPara;

    if(m_pRoutingTile != nullptr)
    {
        delete  m_pRoutingTile;
    }

    this->m_pRoutingTile = new nds::routing::main::RoutingTile();
    //    try {
    m_pRoutingTile->read(bsr, extPara, static_cast<uint8_t>(coordWidth));
    //    }
    //    catch (exception e) {
    //        qDebug() << e.what();
    //        validTile = false;
    //    }

    //    if (!validTile) {
    //        delete m_pRoutingTile;
    //        return -1;
    //    }

    return 0;
}

int Widget::AnalysisBlobAuxTile(const unsigned char *ndsData, uint32_t nBytes)
{
    datascript::BitStreamReader bsr(ndsData,nBytes);
    bool validAuxTile = true;
    datascript::ExtParams extPara;

    if (this->m_pRoutingAuxTile != nullptr)
    {
        delete  m_pRoutingAuxTile;
    }
    this->m_pRoutingAuxTile = new nds::common::flexattr::attrmaps::AttributeLayer();

    //    try{
    m_pRoutingAuxTile->read(bsr, extPara);
    //    }
    //    catch (exception e){
    //        qDebug() << e.what();
    //        validAuxTile = false;
    //    }

    //    if (!validAuxTile)
    //    {
    //        delete m_pRoutingAuxTile;
    //        return -1;
    //    }

    return 0;
}

void Widget::on_listWidget_itemClicked(QListWidgetItem *item)
{
    if (!m_pDBins->isOpen()) {
        std::cout << "Open database Failed！" << std::endl;
        return;
    }
    ui->TileIdList->clear();

    selItem = item->text();
    string selFiled = "";

    if (selItem.compare("routingTileTable") == 0)
    {
        selFiled = "ndsData";
    }
    else if(selItem.compare("routingAuxTileTable") == 0)
    {
        selFiled = "guidanceAttributeLayer";
    }

    QString allIdSql = QString("SELECT Id FROM %1 ;").arg(selItem);

    // select all id
    m_query = m_pDBins->execQuery(allIdSql.toStdString());
    const string filed_id = "id";

    while (!m_query.eof()) {
        int id = m_query.getIntField(filed_id);
        ui->TileIdList->addItem(QString::number(id));
        m_vecAllId.push_back(id);
        m_query.nextRow();
    }

    m_query.finalize();
}

void Widget::on_TileIdList_itemClicked(QListWidgetItem *item)
{
    ui->treeWidget->clear();

    this->m_iTileId = item->text().toInt();
    int coordWidth = CoordWidth(m_iBuildingBlockId, m_iTileId);

    string filed_ndsDataTile = QString("SELECT NdsData from routingTileTable WHERE Id = %1;").arg(item->text()).toStdString();
    std::string ndsDataSQLTile = "ndsData";
    m_query = m_pDBins->execQuery(filed_ndsDataTile);
    int dataLen = 0;
    const unsigned char *dataTile = m_query.getBlobField(ndsDataSQLTile, dataLen);
    AnalysisBlobTile(dataTile, static_cast<uint32_t>(dataLen), coordWidth);
    m_query.finalize();

    std::string filed_ndsDataAuxTile = QString("SELECT guidanceAttributeLayer from routingAuxTileTable WHERE Id = %1;").arg(item->text()).toStdString();
    std::string ndsDataSQLAuxTile  = "guidanceAttributeLayer";
    m_query = m_pDBins->execQuery(filed_ndsDataAuxTile);
    const unsigned char * dataAuxTile = m_query.getBlobField(ndsDataSQLAuxTile,dataLen);
    if (dataAuxTile != nullptr)
    {
        AnalysisBlobAuxTile(dataAuxTile,static_cast<uint32_t>(dataLen));
        m_query.finalize();
    }

    display = new Display(this, ui, this->m_pRoutingTile,this->m_pRoutingAuxTile);

    if (selItem.compare("routingTileTable") == 0)
    {
        display->SetAttrMapListInstance(&(m_pRoutingTile->getAttributeMaps()));
        display->ShowRoutingTile();
    }
    else if (selItem.compare("routingAuxTileTable") == 0)
    {
        display->SetAttrMapListInstance(&(m_pRoutingAuxTile->getAttributeMapList()));
        display->ShowRoutingAuxTile();
    }

    if (dataLen < 0)
    {
        QMessageBox::information(this,"info","This id hasn't guidanceAttributeLayer");
    }

    m_pNdsCommon = new NdsCommon(m_pRoutingTile,m_pRoutingAuxTile,m_pDBins);
}

void Widget::on_addBtn_clicked()
{
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 往 RoutingTileTable 中添加 SPEED_LIMIT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    m_iRefType = ui->refTypeComboBox->currentIndex();
    nds::common::flexattr::valuecodes::AttributeTypeCode attrTypeCode ;
    if (m_strAttrTypeCode.compare("SPEED_LIMIT") == 0)
    {
        attrTypeCode = nds::common::flexattr::valuecodes::AttributeTypeCode::SPEED_LIMIT;
        m_pAddSpeedLimit ->SetContext(m_pNdsCommon);
        m_pAddSpeedLimit->DoWork(m_iTileId,m_iLinkId,m_iRefType);

    }
    else if(m_strAttrTypeCode.compare("PROHIBITED_PASSAGE") == 0)
    {
        attrTypeCode = nds::common::flexattr::valuecodes::AttributeTypeCode::PROHIBITED_PASSAGE;
        m_pAddProhibitedPassageToLink->SetContext(m_pNdsCommon);
        m_pAddProhibitedPassageToLink->DoWork(m_iTileId,m_iLinkId,m_iRefType);
    }
    else if(m_strAttrTypeCode.compare("FLOOR_NUMBER") == 0)
    {
        attrTypeCode = nds::common::flexattr::valuecodes::AttributeTypeCode::FLOOR_NUMBER;
        m_pAddFloorNumber ->SetContext(m_pNdsCommon);
        m_pAddFloorNumber ->DoWork(m_iTileId,m_iLinkId,m_iRefType);
    }
    else if(m_strAttrTypeCode.compare("PARKING") == 0)
    {
        attrTypeCode = nds::common::flexattr::valuecodes::AttributeTypeCode::PARKING;
        m_pAddParkingToLink -> SetContext(m_pNdsCommon);
        m_pAddParkingToLink -> DoWork(m_iTileId,m_iLinkId,m_iRefType);
    }
    else if(m_strAttrTypeCode.compare("ROAD_Z_LEVEL") == 0)
    {
        attrTypeCode = nds::common::flexattr::valuecodes::AttributeTypeCode::ROAD_Z_LEVEL;
        m_pAddRoadZLevel -> SetContext(m_pNdsCommon);
        m_pAddRoadZLevel -> DoWork(m_iTileId,m_iLinkId,m_iRefType);
    }
    else
    {
        attrTypeCode = nds::common::flexattr::valuecodes::AttributeTypeCode::START_OR_DESTINATION_ROAD_ONLY;
        m_pAddStartOrDestinationRoadOnly -> SetContext(m_pNdsCommon);
        m_pAddStartOrDestinationRoadOnly -> DoWork(m_iTileId,m_iLinkId,m_iRefType);
    }

    display->AnalysisAttributeMapList();
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}

Widget::~Widget()
{
    m_iBuildingBlockId = 0;
    m_iLinkId = 0;
    m_iTileId = 0;
    m_iAttrSpeedLimitVal = 0;

    delete m_pRoutingTile;
    m_pRoutingTile = nullptr;

    m_pDBins->close();
    m_pDBins = nullptr;

    // delete m_pAttributeMapItem;
    // m_pAttributeMapItem = nullptr;

    m_vecAllId.clear();
    m_mNdsData.clear();
    m_mNdsDataLen.clear();

    delete ui;
}

void Widget::on_typeComboBox_currentTextChanged(const QString &attrTypeCode)
{
    m_strAttrTypeCode = attrTypeCode;
    if (m_strAttrTypeCode.compare("SPEED_LIMIT") == 0)
    {
        ui->parkingWidget->hide();
        ui->prohibitedPassageWidget->hide();
        ui->startOrDestinationRoadOnlyWidget->hide();
        ui->roadZLevelWidget->hide();
        ui->floorNumberWidget->hide();

        ui->speedLimitWidget->show();
        if(m_pAddSpeedLimit!=nullptr)
        {
            m_pAddSpeedLimit = new AddSpeedLimitToLink(this);
        }
    }
    else if(m_strAttrTypeCode.compare("PROHIBITED_PASSAGE") == 0)
    {
        ui->parkingWidget->hide();
        ui->startOrDestinationRoadOnlyWidget->hide();
        ui->roadZLevelWidget->hide();
        ui->floorNumberWidget->hide();
        ui->speedLimitWidget->hide();

        ui->prohibitedPassageWidget->show();
        m_pAddProhibitedPassageToLink = new AddProhibitedPassageToLink(this);
    }
    else if(m_strAttrTypeCode.compare("FLOOR_NUMBER") == 0)
    {
        ui->prohibitedPassageWidget->hide();
        ui->startOrDestinationRoadOnlyWidget->hide();
        ui->roadZLevelWidget->hide();
        ui->parkingWidget->hide();
        ui->speedLimitWidget->hide();

        ui->floorNumberWidget->show();
        m_pAddFloorNumber = new AddFloorNumberToLink(this);
    }
    else if(m_strAttrTypeCode.compare("PARKING") == 0)
    {
        ui->prohibitedPassageWidget->hide();
        ui->startOrDestinationRoadOnlyWidget->hide();
        ui->roadZLevelWidget->hide();
        ui->floorNumberWidget->hide();
        ui->speedLimitWidget->hide();

        ui->parkingWidget->show();
        m_pAddParkingToLink = new AddParkingToLink(this);
    }
    else if(m_strAttrTypeCode.compare("ROAD_Z_LEVEL") == 0)
    {
        ui->prohibitedPassageWidget->hide();
        ui->startOrDestinationRoadOnlyWidget->hide();
        ui->parkingWidget->hide();
        ui->floorNumberWidget->hide();
        ui->speedLimitWidget->hide();

        ui->roadZLevelWidget->show();
        m_pAddRoadZLevel = new AddRoadZLevel(this);
    }
    else
    {
        ui->prohibitedPassageWidget->hide();
        ui->parkingWidget->hide();
        ui->floorNumberWidget->hide();
        ui->roadZLevelWidget->hide();
        ui->speedLimitWidget->hide();

        ui->startOrDestinationRoadOnlyWidget->show();
        m_pAddStartOrDestinationRoadOnly = new AddStartOrDestinationRoadOnly(this);
    }

    qDebug() << m_strAttrTypeCode ;
}
