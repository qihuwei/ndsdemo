#ifndef ADDSTARTORDESTINATIONROADONLY_H
#define ADDSTARTORDESTINATIONROADONLY_H

#include <QWidget>
#include <QButtonGroup>
#include <NdsCommon.h>
#include <QMessageBox>

namespace Ui {
class AddStartOrDestinationRoadOnly;
}

class AddStartOrDestinationRoadOnly : public QWidget
{
    Q_OBJECT

public:
    explicit AddStartOrDestinationRoadOnly(QWidget *parent = nullptr);
    ~AddStartOrDestinationRoadOnly();

    void    SetContext(NdsCommon * pNdsCommon);

    void    DoWork(int tileId, int linkId,int refType);


    nds::common::flexattr::attrmaps::AttrValueList &BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef);


private:
    Ui::AddStartOrDestinationRoadOnly *ui;

    nds::common::flexattr::attrmaps::AttributeLayer *   m_pRoutingAuxTile =nullptr ;
    nds::routing::main::RoutingTile *                   m_pRoutingTile = nullptr;

    NdsCommon *         m_pNdsCommon = nullptr ;

    QButtonGroup *      m_pGroupBtn = nullptr;

    int m_iTileId = 0;
    int m_iLinkId = 0;

private slots:
        void slotsRadioClick();
};

#endif // ADDSTARTORDESTINATIONROADONLY_H
