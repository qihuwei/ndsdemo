#include "AddProhibitedPassageToLink.h"
#include "ui_AddProhibitedPassageToLink.h"

AddProhibitedPassageToLink::AddProhibitedPassageToLink(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddProhibitedPassageToLink)
{
    ui->setupUi(this);

    m_pSelTabGroupBtn = new QButtonGroup(this);
    m_pSelTabGroupBtn->addButton(ui->routingTileRadioBtn,0);
    m_pSelTabGroupBtn->addButton(ui->routingAuxTileRadioBtn,1);

    // bind Signal
    connect(ui->routingTileRadioBtn,SIGNAL(clicked()),this,SLOT(slotsSelTabRadioClick()));
    connect(ui->routingAuxTileRadioBtn,SIGNAL(clicked()),this,SLOT(slotsSelTabRadioClick()));

    m_pIsInclusiveGroupBtn = new QButtonGroup(this);
    m_pIsInclusiveGroupBtn->addButton(ui->isIncluTureRadioBtn,0);
    m_pIsInclusiveGroupBtn->addButton(ui->isIncluFalseRadioBtn,0);

    // bind Signal
    connect(ui->isIncluTureRadioBtn,SIGNAL(clicked()),this,SLOT(slotSelIsIncluRadioClick()));
    connect(ui->isIncluFalseRadioBtn,SIGNAL(clicked()),this,SLOT(slotSelIsIncluRadioClick()));

    ui->routingTileRadioBtn->clicked();
    SelTableName = "RoutingTileTable";
}

AddProhibitedPassageToLink::~AddProhibitedPassageToLink()
{
    delete ui;
}

void AddProhibitedPassageToLink::slotsSelTabRadioClick()
{
    selTableId = m_pSelTabGroupBtn->checkedId();
    switch (m_pSelTabGroupBtn->checkedId()) {
        case 0:
        SelTableName = "routingTileTable";
        break;
        case 1:
        SelTableName = "routingAuxTileTable";
        break;
    }
}

void AddProhibitedPassageToLink::slotSelIsIncluRadioClick()
{
    switch (m_pIsInclusiveGroupBtn->checkedId()) {
        case 0:
        {
            isInclusive = true;
            break;
        }
        case 1:
        {
            isInclusive = false;
            break;
        }
    }
}

void AddProhibitedPassageToLink::SetContext(NdsCommon *pNdsCommon)
{
    this->m_pNdsCommon = pNdsCommon;

    m_pRoutingTile = m_pNdsCommon->GetRoutingTile();
    m_pRoutingAuxTile = m_pNdsCommon->GetRoutingAuxTile();
}

void AddProhibitedPassageToLink::on_startTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    startTimeOfDay.setHours(static_cast<uint8_t>(dateTime.time().hour()));
    startTimeOfDay.setMinutes(static_cast<uint8_t>(dateTime.time().minute()));
}

void AddProhibitedPassageToLink::on_endTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    endTimeOfDay.setHours(static_cast<uint8_t>(dateTime.time().hour()));
    endTimeOfDay.setMinutes(static_cast<uint8_t>(dateTime.time().minute()));
}

int AddProhibitedPassageToLink::AddFeature2Tile(int nMap, int nEntry, int linkId)
{
    nds::common::flexattr::attrmaps::AttributeMap & refAttrMap = m_pRoutingTile->getAttributeMaps().getAttrMap().elementAt(nMap);
    ObjectArray< nds::common::flexattr::attrmaps::AttributeTypeRef >& refAttrTypeRefArray = m_pRoutingTile->getAttributeMaps().getAttrTypeRef();
    uint8_t value = refAttrMap.getAttrMapType().getValue();

    if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE == value) {
        int bitsMap1 = refAttrMap.bitsizeOf();
        int numEntries = refAttrMap.getNumEntries();
        ObjectArray <nds::common::flexattr::attrmaps::AttrVals4ManyFeatures> entry4ManyFArray;
        entry4ManyFArray.clear();

        ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> featureArray;
        nds::common::flexattr::attrmaps::AttrVals4ManyFeatures entry4ManyF;
        nds::common::flexattr::attrmaps::FeatureReference feature;

        for (int i = 0; i < numEntries; ++i) {
            nds::common::flexattr::attrmaps::AttrVals4OneFeature &refEntry4OneF = refAttrMap.getValues4OneFeature().elementAt(i);

            if (i != nEntry) {
                entry4ManyF.setNumFeatures(1);
            } else {
                entry4ManyF.setNumFeatures(2);
            }

            nds::common::flexattr::attrmaps::FeatureReference &refFeature = refEntry4OneF.getFeature();
            featureArray.add(refFeature);

            if (i == nEntry) {
                nds::common::flexattr::attrmaps::FeatureReference feature;
                feature.setLinkId(static_cast<nds::common::LinkId>(m_iTileId));
                featureArray.add(feature);
            }
            entry4ManyF.setFeature(featureArray);
            nds::common::flexattr::attrmaps::AttrValueList &refAttrValueList = refEntry4OneF.getAttrValList();
            entry4ManyF.setAttrValList(refAttrValueList);
            entry4ManyFArray.add(entry4ManyF);
        }

        refAttrMap.setAttrMapType(nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES);

        refAttrMap.setValues4ManyFeatures(entry4ManyFArray);
        int bitsMap2 = refAttrMap.bitsizeOf();
        // 修改索引号在nMap之后的每个AttributeMap所对应的AttributeTypeRef中的offset字段
        int bitsEntry = bitsMap2 - bitsMap1;
        ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pRoutingTile->getAttributeMaps().getAttrTypeRef();
        nds::common::RelativeBlobOffset offset = 0;
        int numMaps = m_pRoutingTile->getAttributeMaps().getNumMaps();
        for (int i = nMap + 1; i < numMaps; ++i) {
            nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
            offset = refAttrTypeRef.getAttrTypeOffset();
            offset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
            refAttrTypeRef.setAttrTypeOffset(offset);
        }
        // 修改header.externalTileIdListOffset的值
        //        nds::common::RelativeBlobOffset externalTileIdListOffset = m_pRoutingAuxTile->getHeader().getExternalTileIdListOffset();
        //        externalTileIdListOffset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
        //        m_pRoutingTile->getHeader().setExternalTileIdListOffset(externalTileIdListOffset);
    } else if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES == value) {
        bool flag = false;
        for (int i = 0; i < refAttrMap.getNumEntries(); i++) {
            // find an approprate entry and add a feature
            if (refAttrMap.getValues4ManyFeatures().elementAt(i).getAttrValList().getValues().elementAt(1).getTimeRangeOfDay().getStartTime() == startTimeOfDay &&
                refAttrMap.getValues4ManyFeatures().elementAt(i).getAttrValList().getValues().elementAt(1).getTimeRangeOfDay().getEndTime() == endTimeOfDay)
            {
                int bitsMap1 = refAttrMap.bitsizeOf();

                datascript::ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> &featureArray = refAttrMap.getValues4ManyFeatures().elementAt(i).getFeature();
                nds::common::flexattr::attrmaps::FeatureReference feature = m_pNdsCommon->BuildFeature(linkId,refAttrTypeRefArray.elementAt(nMap));

                featureArray.add(feature);
                refAttrMap.getValues4ManyFeatures().elementAt(i).setNumFeatures(static_cast<uint16_t>(featureArray.getSize()));

                int bitsMap2 = refAttrMap.bitsizeOf();
                int bitsEntry = bitsMap2 - bitsMap1;
                //ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pRoutingTile->getAttributeMaps().getAttrTypeRef();
                nds::common::RelativeBlobOffset offset = 0;
                int numMaps = m_pRoutingTile->getAttributeMaps().getNumMaps();
                for (int i = nMap + 1; i < numMaps; ++i) {
                    nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
                    offset = refAttrTypeRef.getAttrTypeOffset();
                    offset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
                    refAttrTypeRef.setAttrTypeOffset(offset);
                }
                // 标记是否找到了合适的 attrVal ,如果没有找到的话，需要新建一个 values4OneFeatures 实体
                flag = true;
            }
        }
        if (!flag)
        {
            return 2;
        }
    }
    else{}

    // update database
    switch (selTableId)
    {
        case 0:
        {
            return m_pNdsCommon->Update2Database(SelTableName,"ndsData",m_iTileId);
        }
        case 1:
        {
            return m_pNdsCommon->Update2Database(SelTableName,"guidanceAttributeLayer",m_iTileId);
        }
    }
}

int AddProhibitedPassageToLink::AddFeature2AuxTile(int nMap, int nEntry, int linkId)
{
    nds::common::flexattr::attrmaps::AttributeMap & refAttrMap = m_pRoutingAuxTile->getAttributeMapList().getAttrMap().elementAt(nMap);
    ObjectArray< nds::common::flexattr::attrmaps::AttributeTypeRef >& refAttrTypeRefArray = m_pRoutingAuxTile->getAttributeMapList().getAttrTypeRef();
    uint8_t value = refAttrMap.getAttrMapType().getValue();

    if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE == value) {
        int bitsMap1 = refAttrMap.bitsizeOf();
        int numEntries = refAttrMap.getNumEntries();
        ObjectArray <nds::common::flexattr::attrmaps::AttrVals4ManyFeatures> entry4ManyFArray;
        entry4ManyFArray.clear();

        ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> featureArray;
        nds::common::flexattr::attrmaps::AttrVals4ManyFeatures entry4ManyF;
        nds::common::flexattr::attrmaps::FeatureReference feature;

        for (int i = 0; i < numEntries; ++i) {
            nds::common::flexattr::attrmaps::AttrVals4OneFeature &refEntry4OneF = refAttrMap.getValues4OneFeature().elementAt(i);

            if (i != nEntry) {
                entry4ManyF.setNumFeatures(1);
            } else {
                entry4ManyF.setNumFeatures(2);
            }

            nds::common::flexattr::attrmaps::FeatureReference &refFeature = refEntry4OneF.getFeature();
            featureArray.add(refFeature);

            if (i == nEntry) {
                nds::common::flexattr::attrmaps::FeatureReference feature;
                feature.setLinkId(static_cast<nds::common::LinkId>(m_iTileId));
                featureArray.add(feature);
            }
            entry4ManyF.setFeature(featureArray);
            nds::common::flexattr::attrmaps::AttrValueList &refAttrValueList = refEntry4OneF.getAttrValList();
            entry4ManyF.setAttrValList(refAttrValueList);
            entry4ManyFArray.add(entry4ManyF);
        }

        refAttrMap.setAttrMapType(nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES);

        refAttrMap.setValues4ManyFeatures(entry4ManyFArray);
        int bitsMap2 = refAttrMap.bitsizeOf();
        // 修改索引号在nMap之后的每个AttributeMap所对应的AttributeTypeRef中的offset字段
        int bitsEntry = bitsMap2 - bitsMap1;
        ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pRoutingAuxTile->getAttributeMapList().getAttrTypeRef();
        nds::common::RelativeBlobOffset offset = 0;
        int numMaps = m_pRoutingAuxTile->getAttributeMapList().getNumMaps();
        for (int i = nMap + 1; i < numMaps; ++i) {
            nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
            offset = refAttrTypeRef.getAttrTypeOffset();
            offset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
            refAttrTypeRef.setAttrTypeOffset(offset);
        }
        // 修改header.externalTileIdListOffset的值
        //        nds::common::RelativeBlobOffset externalTileIdListOffset = m_pRoutingAuxTile->getHeader().getExternalTileIdListOffset();
        //        externalTileIdListOffset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
        //        m_pRoutingTile->getHeader().setExternalTileIdListOffset(externalTileIdListOffset);
    } else if (nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES == value) {
        bool flag = false;
        for (int i = 0; i < refAttrMap.getNumEntries(); i++) {
            // 找到 values4ManyFeatures,添加一个feature
            if (refAttrMap.getValues4ManyFeatures().elementAt(i).getAttrValList().getValues().getData()->getParking().getFacilityType().getValue() == facilityTypeCurIdx&&
                refAttrMap.getValues4ManyFeatures().elementAt(i).getAttrValList().getValues().getData()->getParking().getInOutType().getValue() == inOutTypeCurIdx)
            {
                int bitsMap1 = refAttrMap.bitsizeOf();

                datascript::ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> &featureArray = refAttrMap.getValues4ManyFeatures().elementAt(i).getFeature();
                nds::common::flexattr::attrmaps::FeatureReference feature;
                feature.setLinkId(static_cast<nds::common::LinkId>(linkId));
                feature.setReferenceType(refAttrTypeRefArray.elementAt(nMap).getReferenceType());
                featureArray.add(feature);
                refAttrMap.getValues4ManyFeatures().elementAt(i).setFeature(featureArray);
                refAttrMap.getValues4ManyFeatures().elementAt(i).setNumFeatures(static_cast<uint16_t>(featureArray.getSize()));

                int bitsMap2 = refAttrMap.bitsizeOf();
                int bitsEntry = bitsMap2 - bitsMap1;
                ObjectArray <nds::common::flexattr::attrmaps::AttributeTypeRef> &refAttrTypeRefArray = m_pRoutingAuxTile->getAttributeMapList().getAttrTypeRef();
                nds::common::RelativeBlobOffset offset = 0;
                int numMaps = m_pRoutingAuxTile->getAttributeMapList().getNumMaps();
                for (int i = nMap + 1; i < numMaps; ++i) {
                    nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = refAttrTypeRefArray.elementAt(i);
                    offset = refAttrTypeRef.getAttrTypeOffset();
                    offset += static_cast<unsigned int>(bitsEntry / 8 + ((bitsEntry % 8) > 0 ? 1 : 0));
                    refAttrTypeRef.setAttrTypeOffset(offset);
                }
                // 标记是否找到了合适的 attrVal ,如果没有找到的话，需要新建一个 values4OneFeatures 实体
                flag = true;
            }
        }
        if (!flag)
        {
            return 2;
        }
    }
    else{}

    // update database
    switch (selTableId){
        case 0:{
            return m_pNdsCommon->Update2Database(SelTableName,"ndsData",m_iTileId);
        }
        case 1:{
            return m_pNdsCommon->Update2Database(SelTableName,"guidanceAttributeLayer",m_iTileId);
        }
    }
}

nds::common::flexattr::attrmaps::FeatureReference AddProhibitedPassageToLink::BuildFeature(int linkId, nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef)
{
    nds::common::flexattr::attrmaps::FeatureReference * feature = new nds::common::flexattr::attrmaps::FeatureReference;
    feature->setLinkId(static_cast<nds::common::LinkId>(linkId));
    feature->setReferenceType(entryAttrTypeRef.getReferenceType());
    return *feature;
}

nds::common::flexattr::attrmaps::AttrValueList &AddProhibitedPassageToLink::BuildAttrValList(nds::common::flexattr::attrmaps::AttributeTypeRef &entryAttrTypeRef)
{
    // build one attrValList entry
    datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeValue> attrVals;
    nds::common::flexattr::valuecodes::AttributeValue attrVal1,attrVal2;
    attrVal1.setAttrType(nds::common::flexattr::valuecodes::AttributeTypeCode::PROHIBITED_PASSAGE);

    attrVal2.setAttrType(nds::common::flexattr::valuecodes::AttributeTypeCode::TIME_RANGE_OF_DAY);
    nds::common::flexattr::attrdefs::TimeRangeOfDay timeRangeOfDay;
    timeRangeOfDay.setStartTime(startTimeOfDay);
    timeRangeOfDay.setEndTime(endTimeOfDay);
    attrVal2.setTimeRangeOfDay(timeRangeOfDay);

    attrVals.add(attrVal1);
    attrVals.add(attrVal2);

    nds::common::flexattr::attrmaps::AttrValueList *attrValList = new nds::common::flexattr::attrmaps::AttrValueList;
    attrValList -> setValues(attrVals);
    attrValList->setAttrRefHeader(entryAttrTypeRef);
    return *attrValList;
}

void AddProhibitedPassageToLink::BuildAttrVals4OneFeature(nds::common::flexattr::attrmaps::AttrVals4OneFeature &entry)
{
    nds::common::flexattr::attrmaps::FeatureReference feature;
    nds::common::LinkId linkRef = static_cast<nds::common::LinkId>(m_iTileId);
    feature.setLinkId(linkRef);
    entry.setFeature(feature);

    nds::common::flexattr::valuecodes::AttributeTypeCode attrType1(nds::common::flexattr::valuecodes::AttributeTypeCode::PROHIBITED_PASSAGE);
    nds::common::flexattr::valuecodes::AttributeValue attrValue1;
    attrValue1.setAttrType(attrType1);

    nds::common::flexattr::valuecodes::AttributeTypeCode attrType2(nds::common::flexattr::valuecodes::AttributeTypeCode::TIME_RANGE_OF_DAY);
    nds::common::flexattr::valuecodes::AttributeValue attrValue2;
    attrValue2.setAttrType(attrType2);
    //attrValue.setSpeedLimit(static_cast<nds::common::flexattr::attrdefs::Speed>(m_iAttrSpeedLimitVal));

    ObjectArray <nds::common::flexattr::valuecodes::AttributeValue> &refAttrValueArray = entry.getAttrValList().getValues();
    refAttrValueArray.clear();
    refAttrValueArray.add(attrValue1);
    refAttrValueArray.add(attrValue2);
}

void AddProhibitedPassageToLink::DoWork(int tileId, int linkId, int refType)
{
    m_iTileId = tileId;
    m_iLinkId = linkId;
    int attrTypeRefBitEntry = 0;
    int attrMapBitEntry = 0;
    int bitsEntry = 0;
    int tempNmap = 0;
    int nMap;
    bool updateRet = false;


    ObjectArray< nds::common::flexattr::attrmaps::AttributeMap > attrMapList = m_pRoutingTile->getAttributeMaps().getAttrMap();
    nds::common::flexattr::valuecodes::AttributeTypeCode attrTypeCode1,attrTypeCode2;
    attrTypeCode1 = nds::common::flexattr::valuecodes::AttributeTypeCode::PROHIBITED_PASSAGE;
    attrTypeCode2 = nds::common::flexattr::valuecodes::AttributeTypeCode::TIME_RANGE_OF_DAY;

    datascript::ObjectArray< nds::common::flexattr::valuecodes::AttributeTypeCode> attrVals;
    attrVals.add(attrTypeCode1);
    attrVals.add(attrTypeCode2);

    // routingAuxTileTable
    switch (selTableId)
    {
        case 0:
        {
            m_pNdsCommon->SetAttrMapListInstance(&(m_pRoutingTile->getAttributeMaps()));
            nMap = m_pNdsCommon->FindAttrMapEntryIdx(attrVals, refType);

            // add an AttrMap
            if (nMap == -1)
            {
                // build AttrTypeRef
                nds::common::flexattr::attrmaps::AttributeTypeRef entryAttrTypeRef;
                m_pNdsCommon -> BuildAttrTypeRef(entryAttrTypeRef,attrVals);
                m_pRoutingTile->getAttributeMaps().getAttrTypeRef().add(entryAttrTypeRef);

                // build AttributeMap
                nds::common::flexattr::attrmaps::AttributeMap entryAttrMap;
                nds::common::flexattr::attrmaps::AttrMapType attrMapType = nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES;
                m_pNdsCommon->BuildAttrMap(entryAttrMap,attrMapType,entryAttrTypeRef);
                if (attrMapType == nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE)
                {
                    entryAttrMap.getValues4OneFeature().elementAt(0).setFeature(m_pNdsCommon -> BuildFeature(linkId,entryAttrTypeRef));
                }
                else if(attrMapType == nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_MANY_FEATURES){
                    datascript::ObjectArray <nds::common::flexattr::attrmaps::FeatureReference> featureArray;
                    featureArray.clear();
                    featureArray.add(m_pNdsCommon -> BuildFeature(linkId,entryAttrTypeRef));
                    entryAttrMap.getValues4ManyFeatures().elementAt(0).setFeature(featureArray);
                }
                entryAttrMap.getValues4ManyFeatures().elementAt(0).setAttrValList(BuildAttrValList(entryAttrTypeRef));
                attrMapList.add(entryAttrMap);

                // m_pRoutingTile -> getAttributeMaps().setNumMaps(static_cast<uint16_t>(tempNmap + 1));

                attrTypeRefBitEntry = m_pRoutingTile -> getAttributeMaps().getAttrTypeRef().elementAt(tempNmap).bitsizeOf();
                attrMapBitEntry = m_pRoutingTile -> getAttributeMaps().getAttrMap().elementAt(tempNmap).bitsizeOf();
                bitsEntry = attrTypeRefBitEntry + attrMapBitEntry;

                // 更新新添加的 attrTypeRef 的偏移量
                nds::common::flexattr::attrmaps::AttributeTypeRef &refAttrTypeRef = m_pRoutingTile -> getAttributeMaps().getAttrTypeRef().elementAt(tempNmap);
                nds::common::RelativeBlobOffset offset = m_pRoutingTile -> getAttributeMaps().getAttrTypeRef().elementAt(tempNmap - 1).getAttrTypeOffset();
                offset += (static_cast<uint32_t>((bitsEntry / 8)) + ((bitsEntry % 8) > 0 ? 1 : 0));
                refAttrTypeRef.setAttrTypeOffset(offset);
            }
            else
            {
                nds::common::flexattr::attrmaps::AttrValueList attrValList = BuildAttrValList(m_pRoutingTile->getAttributeMaps().getAttrTypeRef().elementAt(nMap));
                int vals4xxxFeaIdx = m_pNdsCommon->FindVal4xxxFeatureEntryIdx(nMap,attrValList.getValues());
                if (vals4xxxFeaIdx != -1)
                {
                    int addRet = m_pNdsCommon->AddFeature(nMap,vals4xxxFeaIdx,1,this->m_iLinkId);
                    if (addRet == 1)
                        QMessageBox::information(this, "AddResult", "Add Feature Successful!");
                }
                else
                {
                    nds::common::flexattr::attrmaps::FeatureReference feature = m_pNdsCommon->BuildFeature(m_iLinkId,m_pRoutingTile->getAttributeMaps().getAttrTypeRef().elementAt(nMap));
                    if (!m_pNdsCommon -> AddEntry(m_iTileId,nMap,attrVals,feature,attrValList))
                        QMessageBox::critical(this,"AddEntry result","AddEntry failed!");
                }
            }

            updateRet =  m_pNdsCommon -> Update2Database(SelTableName,"ndsData",tileId);
            break;
        }
        case 1:
        {
            updateRet = m_pNdsCommon -> Update2Database(SelTableName,"guidanceAttributeLayer",tileId);
            break;
        }
    }
    if (updateRet)
        QMessageBox::information(this,"Update Database result","Update Database Successful!");

}
